<?php
    function decimalMoneyFormat($price){
        return number_format($price,8,'.',',');
    }

    function generate_app_code($application_id) { 
        $token = getToken(6, $application_id);
        $code = 'EN'. $token . substr(strftime("%Y", time()),2);

        return $code;
    }

    function getToken($length, $seed){    
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "0123456789";

        mt_srand($seed);      // Call once. Good since $application_id is unique.

        for($i=0;$i<$length;$i++){
            $token .= $codeAlphabet[mt_rand(0,strlen($codeAlphabet)-1)];
        }
        return $token;
    }

    function active_class_path($paths, $classes = null)
    {
        foreach ((array) $paths as $path) {
            if (request()->is($path)) {
                return ($classes ? $classes . ' ' : '') . 'active';
            }
        }
        return '';
    }