<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaction');
    }
}
