<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected static $status = [
        0=>'Pending',
        1=>'Approved',
        99=>'Rejected'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function getTransactionStatusView(){
        return self::$status[$this->status];
    }

    public function getAllFicReceiveAttribute(){
        return $this->fic_receive += $this->fic_bonus;
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function wallet()
    {
        return $this->belongsTo('App\Models\Exchange','wallet_type','id');
    }

    public function commission()
    {
        return $this->hasOne('App\Models\Commission');
    }
}
