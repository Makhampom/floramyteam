<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'code', 'email', 'password', 'verify_code', 'verify'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $kycStatus = [
        0=>['style'=>"text-warning",'msg'=>'Pending'],
        1=>['style'=>"text-success",'msg'=>'Approved'],
        99=>['style'=>"text-danger",'msg'=>'Rejected']
    ];

    public function getKycStatusDisplay($html=0){
        $message = self::$kycStatus[$this->kyc_status]['msg'];
        if($html==1){
            return "<p class='".self::$kycStatus[$this->kyc_status]['style']."'>".$message."</p>";
        }else{
            return $message;
        }
    }

    public function referring()
    {
    return $this->belongsToMany('App\Models\User', 'user_referrer', 'user_id', 'referrer_id')->withPivot('status')
    ->withTimestamps();
    }

    // Same table, self referencing, but change the key order
    public function referrer()
    {
    return $this->belongsToMany('App\Models\User', 'user_referrer', 'referrer_id', 'user_id')->withPivot('status')
    ->withTimestamps();
    }


    public function getUsernameAttribute(){
        return $this->name.' '.$this->surname;
    }

    public function transaction()
    {
        return $this->hasMany('App\Models\Transaction');
    }

    public function commission()
    {
        return $this->hasMany('App\Models\Commission');
    }

    public function country()
    {
    return $this->belongsTo('App\Models\Country');
    }
}
