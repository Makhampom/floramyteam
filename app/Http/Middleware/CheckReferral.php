<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;

class CheckReferral
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
        //dd($request->cookie());
        if( $request->query('ref') ) {
            $user = \App\Models\User::where('code',$request->query('ref'))->first();
            if($user){
                $user->ref_count += 1;
                $user->save();
                return redirect('/')->withCookie(Cookie::forever('referral', $request->query('ref')));
            }
        }
        
        return $next($request);
    }
}
