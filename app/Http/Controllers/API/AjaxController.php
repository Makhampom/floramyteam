<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;

class AjaxController extends Controller
{
    public function checkUserEmail(Request $request){
        if (!empty($request) && $request->type == "email" && $request->email != '' && filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email',$request->email)->count();
            if($user == 0){
                $rs['stt'] = "success";                 
                $rs['fetch'] = User::where('email',$request->email)->count();
            }else{
                $rs['stt'] = 'failed';
                $rs['ermsg'] = 'Email address already in use.';
            }
        }else{
            $rs['stt'] = 'failed';
            $rs['ermsg'] = 'Email is invalid.';
        }
        return response()->json($rs);
    }

    public function chkExistingEmail(Request $request){
        if (!empty($request) && $request->type == "email" && $request->email != '' && filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email',$request->email)->count();
            if($user == 1){
                $rs['stt'] = "success";                 
                $rs['fetch'] = User::where('email',$request->email)->count();
            }else{
                $rs['stt'] = 'failed';
                $rs['ermsg'] = 'Email address does not in use.';
            }
        }else{
            $rs['stt'] = 'failed';
            $rs['ermsg'] = 'Email is invalid.';
        }
        return response()->json($rs);
    }
}
