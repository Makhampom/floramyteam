<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use App\Models\Transaction;

class WalletController extends Controller
{
    public function index(){
        return view('frontend.my_wallet');
    }

    public function setWallet(Request $request){
        if($request->eth_wallet != ''){
            Auth::user()->eth_wallet = $request->eth_wallet; 
            Auth::user()->save();
        }
        return back();
    }
}
