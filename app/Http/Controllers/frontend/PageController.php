<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;

use App\Models\Exchange;

class PageController extends Controller
{
    public function index(){
        $list = Exchange::get();
        return view('frontend.index',compact('list'));
    }

    public function testmail(){
        Mail::send('emails.welcome',[], function($message){
            $message->to('greeneat2@hotmail.com', 'John Smith')->subject('Welcome!');
        });

        if (Mail::failures()) {
            dd(Mail::failures());
        }
    }
}
