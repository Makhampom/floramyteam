<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use Illuminate\Support\Facades\Redirect;

use App\Models\Exchange;
use App\Models\Transaction;
use App\Models\Commission;

class OrderController extends Controller
{
    public function index($type){
        if(Auth::user()->eth_wallet == ''){
            return redirect('/profile?a=set-wallet');
        }
        $product = Exchange::where('code',$type)->first();
        if($product){
            return view('frontend.product_data',compact('product'));
        }else{
            return redirect('/'); 
        }
    }

    public function store(Request $request,$type){
        $product = Exchange::where('code',$type)->first();
        if($product){
            $validator = $this->orderValidator($request->all());
            if ($validator->fails()) {
                return Redirect::back()
                            ->withErrors($validator)
                            ->withInput();
            }else{
                $transaction = new Transaction;
                $transaction->user_id = Auth::user()->id;
                $transaction->wallet_type = $product->id;
                $transaction->transfer_code = $request->transfer_code;
                $transaction->to_wallet = $product->wallet;
                $transaction->send_value = $request->input_coin;
                $transaction->fic_rate = $product->rate;
                $transaction->bonus_rate = $product->bonus;
                //$transaction->fic_receive = bcmul($request->input_coin,$product->rate,8);
                $cal_fic_receive = $request->input_coin*$product->rate;
                $transaction->fic_receive = number_format($cal_fic_receive,8,'.',''); 

                $cal_fic_bonus = $transaction->fic_receive * $transaction->bonus_rate;
                $transaction->fic_bonus = number_format($cal_fic_bonus/100, 8,'.','');

                $transaction->ttl_receive = $cal_fic_receive+($cal_fic_bonus/100);

                $transaction->save();
  
                if(Auth::user()->referring->isNotEmpty()){
                    $commission = new Commission;
                    $commission->user_id = Auth::user()->referring->first()->id;
                    $commission->transaction_id = $transaction->id;
                    $commission->fic = number_format(($transaction->fic_receive * 3.5)/100, 8,'.','');
                    $commission->save();
                }
                return redirect('/my-wallet');
            }
            //return view('frontend.productData',compact('product'));
        }else{
            return redirect('/'); 
        }

    }

    public function confirm(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:transactions,id',
            'token' => 'required'
        ]);
        if ($validator->passes()) {
            $order = Transaction::where('id',$request->id)->where('user_id',Auth::user()->id)->first();
            if($order){
                $order->transfer_code = $request->token;
                $order->save();
                return response()->json(['status'=>'success']); 
            }else{
                return response()->json(['status'=>'error']);
            }
        }else{
            response()->json(['status'=>'error','error'=>$validator->errors()->all()]);
        }
    }

    protected function orderValidator(array $data)
    {
        return Validator::make($data, [
            'input_coin' => 'required|numeric',
            'product_type' => 'required|string|max:5|exists:exchanges,code',
        ]);
    }
}
