<?php

namespace App\Http\Controllers\frontend;

use Crypt;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PragmaRX\Google2FA\Google2FA;
use Illuminate\Foundation\Validation\ValidatesRequests;
use \ParagonIE\ConstantTime\Base32;
use Auth;

class Google2FAController extends Controller
{

    public function getTFAToken(Request $request){
        //generate new secret
        $google2fa = new Google2FA();
    
        $secret = $google2fa->generateSecretKey();
        //$secret = $this->generateSecret();

        //get user
        $user = Auth::user();

        //encrypt and then save secret
        //$user->google2fa_secret = Crypt::encrypt($secret);
        $user->google2fa_secret = $secret;
        $user->google2fa_status = 0;
        $user->save();

        //generate image for QR barcode
        $imageDataUri = $google2fa->getQRCodeInline(
            $request->getHttpHost(),
            $user->email,
            $secret,
            200
        );

        return response()->json([
            'token' => $secret,
            'image' => $imageDataUri
        ]);
    }

    public function postValidateToken(Request $request){
        $return = false;
        if($request->totp != ''){
            $google2fa = new Google2FA();
            $user = Auth::user();
            $secret = $request->totp;
            $window = 8;
            $valid = $google2fa->verifyKey($user->google2fa_secret, $secret, $window);
            if($valid){
                $user->google2fa_status = 1;
                $user->save();    
            }
            $return = $valid; 
        }
        return response()->json(['status' => $return]);
    }

    public function disableTwoFactor(Request $request){
        $return = false;
        $user = Auth::user();
        if($request->token != '' && $request->token == $user->google2fa_secret){
            $user->google2fa_secret = '';
            $user->google2fa_status = 0;
            $user->save();
            $return = true;
        }
        return response()->json(['status' => $return]);
    }
}
