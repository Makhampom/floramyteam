<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Crypt;
use \ParagonIE\ConstantTime\Base32;
use PragmaRX\Google2FA\Google2FA;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Auth;


class SettingController extends Controller
{
    use ValidatesRequests;

    public function index(Request $request){
        $user = Auth::user();
        return view('frontend.setting',compact('user'));
    } 
    
    /**
     * Generate a secret key in Base32 format
     *
     * @return string
     */
    private function generateSecret()
    {
        $randomBytes = random_bytes(10);

        return Base32::encodeUpper($randomBytes); 
    }
}
