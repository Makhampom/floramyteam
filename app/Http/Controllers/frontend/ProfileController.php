<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Hash;
use File;

class ProfileController extends Controller
{
    public function index(){
    	return view('frontend.profile');
    }

    public function update(Request $request){
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $user = Auth::user();
            $user->name = $request->name;
            $user->surname = $request->surname;
            $user->address = $request->address;
            $user->city = $request->city;
            $user->countries_id = $request->country;
            $user->save();
        }
        return Redirect::back();
    }

    public function changePassword(Request $request){
        if (!(Hash::check($request->old_password, Auth::user()->password))) {
            // The passwords matches
            return redirect('/profile#changePassword')->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($request->old_password, $request->new_password) == 0){
            //Current password and new password are same
            return redirect('/profile#changePassword')->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
 
        $validatedData = $request->validate([
            'old_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
        ]);
 
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->new_password);
        $user->save();
        return redirect('/profile#changePassword')->with("success","Password changed successfully !");
    }

    public function uploadKYC(Request $request){
        $validatedData = $request->validate([
            'kyc_file' => 'required|mimes:jpeg,png,jpg,svg,pdf',
        ]);
        if ($request->hasFile('kyc_file')) {
            $path = '/images/kyc/';
            $user = Auth::user();
            if($user->kyc_file != ''){
                $file_path = public_path($path.$user->kyc_file);
                if (File::exists($file_path)) {
                    File::delete($file_path);
                }
                $user->kyc_file = '';
                $user->save(); 
            }
            $image = $request->file('kyc_file');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path($path);
            $image->move($destinationPath, $input['imagename']);
            $user->kyc_file = $input['imagename'];
            $user->kyc_status = 0;
            $user->save();
            return back()->with('kyc_success','Your KYC image is successfully uploaded.');
        }else{
            return back()->with('kyc_error','Your KYC image file is required.');
        }
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string',
            'surname' => 'required|string',
            'country' => 'required|numeric|exists:countries,id',
        ]);
    }
}
