<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Cookie;
use Illuminate\Support\Facades\Validator;
use PragmaRX\Google2FA\Google2FA;

use Cache;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Http\Requests\ValidateSecretRequest;

use App\Models\User;

class UserLoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:web', ['except' => ['logout']]);
    }
    public function showLoginForm(Request $request)
    {
      return view('auth.login',['guard'=>'user','location'=>url('/member/login')]);
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
          'email'   => 'required|email',
          'password' => 'required|min:6',
        ]);
    }
    public function login(Request $request)
    {
      $valid = true;
      // Validate the form data
      $validator = $this->validator($request->all());
      if (!$validator->fails()) {
        $user = User::where('email',$request->email)->first();
        if($user){
          if($user->google2fa_secret != '' && $user->google2fa_status == 1){
            if($request->totp != ''){
              $google2fa = new Google2FA();
              $secret = $request->totp;
              $window = 8;
              $codeCheck = $google2fa->verifyKey($user->google2fa_secret, $secret, $window);
              if($codeCheck == false){
                $valid = false;
                $validator->errors()->add('totp', 'Your 2 Factor Autentication is wrong!');
              }
            }else{
              $valid = false;
              $validator->errors()->add('totp', 'Your 2 Factor Autentication is required!');
            }
          }
        }else{
          $valid = false;
          $validator->errors()->add('error', 'Your Email Address or Password is wrong!');
        }
      }

      if($valid){
        if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password, 'verify' => 1], $request->remember)) {
          return redirect('/');
        }else{
          $validator->errors()->add('email', 'Your need to verify your email first!');
        }
      }
      $validator->errors()->add('login', 'Something went wrong!');
      return redirect()->back()->withErrors($validator)->withInput($request->only('email', 'remember'));
    }

    public function logout(){
      Auth::logout();
      return redirect('/');
    }

    public function getValidateToken()
    {
        if (session('2fa:user:id')) {
            return view('2fa/validate');
        }

        return redirect('login');
    }

    public function postValidateToken(ValidateSecretRequest $request)
    {
        //get user id and create cache key
        $userId = $request->session()->pull('2fa:user:id');
        $key    = $userId . ':' . $request->totp;

        //use cache to store token to blacklist
        Cache::add($key, true, 4);

        //login and redirect user
        Auth::loginUsingId($userId);

        return redirect()->intended($this->redirectTo);
    }
}