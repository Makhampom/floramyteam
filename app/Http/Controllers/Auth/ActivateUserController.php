<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Auth;
use Cookie;

use App\Models\User;

class ActivateUserController extends Controller
{
    public function index($email,$token){
        if($email != '' && $token != ''){
            $user = User::where('email',$email)->where('verify_code',$token)->first();
            if($user){
                $user->verify = 1;
                $user->save();
            }
        }
        return redirect('/'); 
    }
}
