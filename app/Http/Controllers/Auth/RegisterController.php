<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Auth;
use Cookie;
use Mail;

use App\Models\User;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'country' => 'nullable|exists:countries,id',
            /*'eth_wallet' => 'required|string|max:255|unique:users,eth_wallet',*/
            'email' => 'required|string|email|max:255|unique:users,email|confirmed',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {   
        $verify_code = str_random(10);

        return User::create([
            'name' => $data['name'],
            'surname'=>$data['lastname'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'verify_code' => $verify_code,
            //'code' => bcrypt(str_random(6))
        ]);


    }

    public function register(Request $request){
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $validator->errors()->add('register', 'Something went wrong!');
            return  back()
                    ->withErrors($validator)
                    ->withInput();
        }else{
            //dd($request->all());
            $user = $this->create($request->all());
            $user->eth_wallet = $request->eth_wallet;
            $user->countries_id = $request->country;
            $user->code = getToken('6',$user->id);
            $user->save();
            //dd($user);
            //Auth::guard('web')->login($user);
            $reditect = url('https://support.florafic.io/mail/send_mail_active.php?email='.$user->email.'&verify_code='.$user->verify_code);
            if($request->cookie('referral') != ''){
                $referrer = User::where('code',$request->cookie('referral'))->first();
                if($referrer){
                    $referrer->referrer()->attach($user->id);
                    return redirect($reditect)->withCookie(Cookie::forget('referral'));
                }
            }else{
                return redirect($reditect);
            }
        }
    }

    public function verifly_regis(){
        return view('auth.verifly_regis');
    }
}
