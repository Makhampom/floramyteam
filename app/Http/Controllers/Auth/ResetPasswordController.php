<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use DB;
use Auth;

use App\Models\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset(Request $request){
        $valid = DB::table(config('auth.passwords.users.table'))->where('email',$request->email)->where('token',$request->token)->first();
        if($valid){
            $user = User::where('email',$request->email)->first();
            if($user){
                $user->password = bcrypt($request->password);
                $user->save();

                DB::table(config('auth.passwords.users.table'))->where('email',$request->email)->where('token',$request->token)->delete();
                Auth::guard('web')->login($user);
                return redirect('/');
            } 
        }
        return redirect()->back()->withErrors();
    }
}
