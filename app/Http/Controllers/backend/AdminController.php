<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use App\Models\Transaction;
use App\Models\User;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth:admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.admin');
    }

    public function viewTransactionData($id){
        $transaction = Transaction::find($id);
        return view('backend.transaction_detail',compact('transaction'));
    }

    public function acceptOrder(Request $request,$id){
        if($id == $request->id){
            $transaction = Transaction::where('id',$request->id)->where('status',0)->with('wallet')->with('user')->first();
            if($transaction){
                if($transaction->status == 0){
                    if($request->send_type == 1){
                        $send_amount = $transaction->send_value;      
                    }else if($request->send_type == 2){
                        $send_amount = $request->actually_coin;
                    }
                    

                    $fic_amount = bcmul($send_amount,$transaction->fic_rate,8);
                    $fic_bonus = number_format(($fic_amount * $transaction->bonus_rate)/100, 8,'.','');
                    $all = bcadd($fic_amount,$fic_bonus,8);
                    $fic_all = bcadd($fic_amount,$fic_bonus,8);
                    
                    $transaction->actual_send = number_format($send_amount,8,'.','');
                    $transaction->actual_fic = number_format($fic_amount,8,'.','');
                    $transaction->actual_bonus = number_format($fic_bonus,8,'.','');
                    $transaction->actual_fic_recieve = number_format($fic_all,8,'.','');
                    $transaction->status = 1;
                    $transaction->save();
                
                    $userFIC = $transaction->user->fic;
                    $transaction->user->fic = number_format(($userFIC+$fic_all),8,'.',''); 
                    $transaction->user->save();

                    if($transaction->commission){
                        if($transaction->user->referring[0]->pivot->status == 0){
                            $transaction->commission->fic = number_format(($fic_amount * 3.5)/100, 8,'.','');
                            $transaction->commission->status = 1;
                            $transaction->commission->save();
                            
                            $userCommission = $transaction->commission->user->fic; 
                            $transaction->commission->user->fic = number_format(($userCommission+$transaction->commission->fic),8,'.','');
                            $transaction->commission->user->save();
                            
                            $transaction->user->referring[0]->pivot->status = 0;
                            $transaction->user->referring[0]->pivot->save();
                        }
                    }
                }
                return redirect('/cmscontrol');
            }else{
                return back();
            }
        }else{
            return back();
        }
    }

    public function rejectOrder($id){
        if($id != ''){
            $order = Transaction::find($id);
            if($order){
                $order->status = 99;
                $order->save();
            }
        }
        return back();
    }


    public function viewKyc(){
        //return "Test viewKyc";
        return view('backend.kyc-list');
    }
    public function approveKyc($id){
        $user = User::where('code',$id)->first();
        if($user){
            $user->kyc_status = 1;
            $user->save();
        }

        return back();
    }
    public function rejectKyc($id){
        $user = User::where('code',$id)->first();
        if($user){
            $user->kyc_status = 99;
            $user->save();
        }

        return back();
    }
}
