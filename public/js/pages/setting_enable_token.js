 $( document ).ready(function() {
    $('#enableTFA').click(function(){
      if($('#show-2fa-form').is(":visible") === false){
        $.ajax({
          url: "/2fa/getTFAData",
          method: "POST",
        }).done(function( response ) {
          $('img#TFA_key').attr('src',response.image);
          $('input#TFA_key').attr('placeholder',response.token);
          $('span#TFA_key').html(response.token);
        }).fail(function( jqXHR, textStatus ) {
          alert( "Request failed: " + textStatus );
        });
      }
      $('#show-2fa-form').toggle('collapse');
    });

    $("#submit").click(function(event) {
      var totp = $('input#totp').val();
      if(totp == '' || totp.length < 6){
        inputValidate('input#totp');
      }else{
        $.ajax({
          url: "/2fa/validate",
          method: "POST",
          data:{totp:totp}
        }).done(function( response ) {
          if(response.status==false){
            inputValidate('input#totp');
          }else{
            location.reload();   
          }
        }).fail(function( jqXHR, textStatus ) {
          alert( "Request failed: " + textStatus );
        });
      }
    });
  });