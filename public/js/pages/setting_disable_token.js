 $( document ).ready(function() {
    $('#enableTFA').click(function(){
      $('#show-2fa-form').toggle('collapse');
    });

    $("#submit").click(function(event) {
      var token = $('input#token').val();
      if(token == ''){
        inputValidate('input#token');
      }else{
        $.ajax({
          url: "/2fa/disable",
          method: "POST",
          data:{token:token}
        }).done(function( response ) {
          if(response.status==false){
            inputValidate('input#token');
          }else{
            location.reload();   
          }
        }).fail(function( jqXHR, textStatus ) {
          alert( "Request failed: " + textStatus );
        });
      }
    });
  });