$(document).ready(function() {
	
	//$('#input_btc').maxlength({max: 10});
	$('#submit_order').attr({
		disabled: 'disabled'
	});

	$('#input_coin').maxlength();
	$('#receive_fic').maxlength();
	$('#fic_include_bonus').maxlength();
	
	//alert(btc_per_fic);
	$('#input_coin').keyup(function(event) {
		input_send_curreny();
	});

	$('#input_coin').change(function(){
		input_send_curreny();
	});

	$('#receive_fic').keyup(function(event) {
		//fic_convert_curreny();
	});

	$('#receive_fic').change(function(){
		//fic_convert_curreny();
	});

	function fic_convert_curreny(){
		var currency_per_fic = $('#input_coin').data('per-fic');
		var ttl_fic_recive = $('#receive_fic').val();
		if(isNaN(ttl_fic_recive)){
			ttl_fic_recive = 0;
		}
		var send_value = (parseFloat(ttl_fic_recive)/parseFloat(currency_per_fic)).toFixed(8);
		if(isNaN(send_value)){
			send_value = 0;
		}
		$('#input_coin').val(send_value);

		var bonus = $('#current_bonus').val();
		var bonus_percent = ttl_fic_recive*(bonus/100);
		var fic_include_bonus = (parseFloat(ttl_fic_recive)+parseFloat(bonus_percent)).toFixed(8);

		$('#fic_include_bonus').val(fic_include_bonus);

		activateButton(send_value);
	}

	function input_send_curreny(){
		var currency_per_fic = $('#input_coin').data('per-fic');
		var send_value = $('#input_coin').val();
		if(isNaN(send_value)){
			send_value = 0;
		}
		var ttl_fic_recive = (parseFloat(send_value)*parseFloat(currency_per_fic)).toFixed(8);
		if(isNaN(ttl_fic_recive)){
			ttl_fic_recive = 0;
		}
		$('#receive_fic').val(ttl_fic_recive);

		var bonus = $('#current_bonus').val();
		var bonus_percent = ttl_fic_recive*(bonus/100);

		var fic_include_bonus = (parseFloat(ttl_fic_recive)+parseFloat(bonus_percent)).toFixed(8);

		$('#fic_include_bonus').val(fic_include_bonus);

		activateButton(send_value);
	}

	$('#comp_wallet_qr').click(function(event) {
		$( this ).toggleClass( 'img_small', 'img_large' );
	});
});

function activateButton(send_value){
	if (send_value <= 0) {
		$('#submit_order').attr({
			disabled: 'disabled'
		});
	}

	if (send_value > 0) {
		$('#submit_order').removeAttr('disabled');
	}
}

