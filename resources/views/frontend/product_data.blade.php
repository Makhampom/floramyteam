@extends('layouts.buywith')
@section('content')
<div class="row">
    <div class="col-md-6" style="margin-left: 1em; margin-top: 1em; color: #000; font-weight: bold; font-size: 1.5em; padding-left: 5px;">Calculate your investment amount</div>
</div>  
<form name="orderFIC" id="orderFIC" action="/order/{{$product->code}}" method="POST">
<div class="row mt-3">
<div class="col col-md-7 mt-2">
    <div class="col">
        <div class="card heigh2 heigh1">
          <div class="card-body">
            <p class="ml5">Buy FIC Token</p>
            <div class="row ">
              <!-- show exstra small -->
              <div class="d-xl-none d-sm-none d-md-none col-12">
                <div class="row">
                  <div class="col-3"></div>
                  <div class="select-buy-currency text-center col-6">
                    <img src="{{asset('images/'.$product->logo)}}">
                    <div class="top-em1">{{$product->code}}</div>
                  </div>
                </div>
              </div>
              <div class="d-xl-none d-sm-none d-md-none col-12">
                <div class="arrow-convert text-center mt-2 mb-2">
                  <img src="{{asset('images/arrow-convert-down.png')}}" class="ex-img">
                </div>
              </div>
              <div class="d-xl-none d-sm-none d-md-none col-12">
                <div class="row">
                  <div class="col-3"></div>
                  <div class="select-buy-currency text-center col-6">
                    <img src="{{asset('images/fic-currency.png')}}">
                    <div class="top-em1">FIC</div>
                  </div>
                </div>
              </div>
            </div>

            <!-- hidden exstra small -->
            <div class="row">
              <div class="col-md-1"></div>
              <div class="d-xl-block d-none d-md-block text-center col-md-4">
                <div class="col-md-12 select-buy-currency">
                  <img src="{{asset('images/'.$product->logo)}}" class="img-fluid mt-4">
                  <div class="top-em1">{{$product->code}}</div>
                </div>
              </div>
              <div class="d-xl-block d-none d-md-block col-md-2">
                <div class="text-center mg-top">
                  <img src="{{asset('images/arrow-convert.png')}}" class="img-fluid">
                </div>
              </div>
              <div class="d-xl-block d-none d-md-block text-center col-md-4">
                <div class="col-md-12 select-buy-currency">
                  <img src="{{asset('images/fic-currency.png')}}" class="img-fluid">
                  <div class="top-em1">FIC</div>
                </div>
              </div>
              <div class="col-md-1"></div>
            </div>
            <div class="row justify-content-center">
              <div class="col-12 col-md-1"></div>
              <div class="col-12 col-md-4 mt-4 box-buy">
                <h6>You send:</h6>
                   <div class="input-group">
                    <input type="text" name="input_coin" class="form-control" id="input_coin" data-per-fic="{{$product->rate}}" value="0.00000000" style="font-size: 13px;" data-parsley-send-validate="" max="1000" step="10" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="number">
                    <div class="input-group-prepend">
                        <div class="input-group-text" style="padding-left: 1px; padding-right: 1px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;">{{$product->code}}</div>
                    </div>
                  </div>     
              </div>
              <div class="col-12 col-md-2"></div>
              <div class="col-12 col-md-4 mt-4 box-buy">
                <h6>You receive:</h6>
                  <div class="input-group">
                    <input type="text" class="form-control" id="receive_fic" name="receive_coin" value="0.00000000" style="font-size: 13px;" step="10" min="100" data-parsley-validation-threshold="1" data-parsley-trigger="keyup" data-parsley-type="number" disabled>
                    <div class="input-group-prepend">
                        <div class="input-group-text" style="padding-left: 1px; padding-right: 1px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;">FIC</div>
                    </div>
                  </div>
              </div>
              <div class="col-12 col-md-1"></div>
            </div>    
            <div class="row justify-content-center">
              <div class="col-12 col-md-1"></div>
              <div class="col-12 col-md-4 mt-4 box-buy">
                <h6>Your bonuses:</h6>
                  <div class="input-group">
                    <input type="text" class="form-control" disabled="disabled" value="{{$product->bonus}}%" style="font-size: 13px; color: #f00; border-top-right-radius: 4px; border-bottom-right-radius: 4px;">
                    <input type="hidden" id="current_bonus" value="{{$product->bonus}}" name="">
                  </div>
              </div>
              <div class="col-12 col-md-2"></div>
              <div class="col-12 col-md-4 mt-4 box-buy">
                <h6>Including bonuses:</h6>
                  <div class="input-group">
                    <input type="hidden" id="temp" name="">
                    <input type="text" class="form-control" disabled="disabled" id="fic_include_bonus" name="receive_coin_include_bonus" value="0.00000000" style="font-size: 13px;">
                    <div class="input-group-prepend">
                        <div class="input-group-text" style="padding-left: 1px; padding-right: 1px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;">FIC</div>
                    </div>
                  </div>
              </div>
              <div class="col-12 col-md-1"></div>
            </div>            
          </div>
        </div>
      </div>
    </div><!-- col-md-7 -->

    <div class="col-12 col-md-5 mt-2">
      <div class="col col-md-12">
        <div class="card heigh1">
          <div class="card-body">
            <div class="" style="color: #000;">
              <h5>5 Steps Guide</h5>
            </div>
            <div class="text-16">1. Calculate how much coins you want to buy</div>
            <div class="text-16 top-em1">2. Copy/Scan the displayed address</div>
            <div class="text-16 top-em1">3. Send your cryptocurrency from any wallet you like (also exchange wallet is ok)</div>
            <div class="text-16 top-em1">4. After the payment click SUBMIT ORDER and we keep you posted with status e-mails, so check your mails</div>
            <div class="text-16 top-em1">5. Coins will be send to your FloraFIC wallet. 
              <span style="color: #f00;">Due to Pre ICO duration, you will receive FIC token in the ICO day.</span>
            </div>
            <br><br><br><br>
          </div>
        </div>
      </div>
    </div><!-- col-md-5 -->
</div> <!-- ROW 1 -->

<div class="row mt-3">
    <div class="col-12 col-md-7">
        <div class="col-12 col-md-12"><h5>Current exchange rate: {{$product->code}}/FIC =  1/{{$product->rate}}</h5></div>
        <div class="col-12 col-md-12">Copy the address below and make the payment of  <span>{{$product->code}}</span></div>

        <div class="col-12 col-md-12 mt-3 table-responsive">
          <table class="table table-bordered">
            <thead class="bg-white">
              <tr>
                <th scope="col">Address</th>
                <th scope="col" class="text-center">QR Code</th>
              </tr>
            </thead>
            <tbody class="bg-light">
              <tr>
                <td scope="row" class="align-middle">{{$product->wallet}}</td>
                <td class="text-center"><img src="{{asset('images/'.$product->wallet_qr)}}" id="comp_wallet_qr" class="img_small img-thumbnail"></td>
              </tr>
            </tbody>
          </table>
          <!-- <img src="{{asset('images/'.$product->wallet_qr)}}" id="img_test" class=" qr-img img-thumbnail">
          
          <div id="effect" class="newClass ui-corner-all">
              Etiam libero neque, luctus a, eleifend nec, semper at, lorem. Sed pede.
          </div> -->

        </div>
    </div>      
</div><!-- ROW 2 -->


<div class="row">
  <div class="col-12 col-md-7 mt-3">
    <div class="col-12 col-md-12">
      <h6>Attach your address transection id</h6>
      <textarea class="form-control" id="transfer_code" name="transfer_code" rows="1"></textarea>
    </div>
  </div>
</div>

<div class="row mb-3 mt-3">
  <div class="col-12 col-md-7">
      <div class="col-12 col-md-12">After you have send the payment to the address above please click on submit order</div>
  </div>

  <div class="col-12 col-md-7 text-center">
    <div class="col-12 col-md-12 text-center">
        <br>
        <input type="hidden" name="product_type" value="{{$product->code}}">
        <button type="button" class="btn btn-warning btn-lg col-12 col-md-12" id="submit_order" style="color: #fff;">
          <i class="fa fa-shopping-cart" aria-hidden="true"></i> &nbsp;Submit Order
        </button>
    </div>
  </div>

  </div>  
</div>
<!-- ROW 3 -->

{{ csrf_field() }}
</form>
@endsection
@section('style')
<style>
.parsley-errors-list li.parsley-required {
    padding: 10px;
    color: #f00;
}
</style>
@endsection
@section('script')
<script src="{{asset('js/buy-script.js')}}"></script>
<script src="{{asset('js/jquery.plugin.js')}}"></script>
<script src="{{asset('js/jquery.maxlength.js')}}"></script>
{!! HTML::script('js/parsley.js'); !!}
<script>
    $(document).ready(function() {
        $('#submit_order').click(function(){
            $('#orderFIC').submit();
        });
    });
    /*$('#orderFIC').parsley();receive
    window.Parsley.addValidator('sendValidate', {
    requirementType: 'number',
    validateNumber: function(value) {
      if(value > 1001){
        return false;
      }
      return true
    },
    messages: {
      //en: 'If you want to invest more than 1,000 {{$product->code}}. Please contact via email for get special offer'
    }
  }).addValidator('receiveValidate', {
    requirementType: 'number',
    validateNumber: function(value) {
      if(value > 100){
        return false;
      }
      return true
    },
    messages: {
      //en: 'If you want to invest more than 100 FIC. Please contact via email for get special offer'
    }
  });*/
</script>
@endsection