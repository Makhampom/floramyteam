@extends('layouts.default')
@section('content')
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12" style="margin-left: 1em; margin-top: 1em; color: #000; font-weight: bold; font-size: 1.5em; padding-left: 5px;">Your Countrol Panel Here</div>
        </div>  

        <div class="row">
          <div class="col-12 col-md-10 div-white" style="margin-bottom: 2em;">
            <div class="row">
              <div class="col-md-12 mt-3">
                <h5>Your Profile Hello, {{Auth::user()->name}}</h5>
                <br>
                <h5>Current balance: 
                @if(Auth::user()->fic > 0)
                  {{number_format(Auth::user()->fic,8,'.',',')}}
                @else
                  0.00000000
                @endif
                 FIC</h5>
              </div>
              <div class="col-md-12">
                <h5>ETH Wallet : {!!(Auth::user()->eth_wallet)?Auth::user()->eth_wallet:'
                  <span class="text-danger">Please set your wallet.</span>'!!}
                </h5>
                @if(Auth::user()->eth_wallet != '')
                <button class="btn btn-dark mx-sm-2 mb-4 edit-button mt-3" style="padding-left: -2em;">Edit</button>
                @endif
              </div>
              <div style="display:{{(Auth::user()->eth_wallet)?'none':'block'}}" id="edit-wallet-form">
                <div class="col-md-12">
                  Input your ETH wallet address for recive FIC
                </div>
                <form class="form-inline" action="{{URL::to('/setWallet')}}" method="post">
                {{ csrf_field() }}
                  <div class="form-group mx-sm-3 mb-4">
                    <input type="text" class="form-control" value="{{Auth::user()->eth_wallet}}" name="eth_wallet" id="address" placeholder="Your ETH Wallet" required="required" style="width: 18em;">
                  </div>
                  <button type="submit" class="btn btn-success mx-sm-2 ml-2 mb-4">Save</button>
                </form>
              </div>
            </div>

            <form class="mt-3" method="POST" action="/profile/update" id="profile-form">
              {!! csrf_field() !!}
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputName">Name</label>
                  <input type="text" class="form-control" id="name" placeholder="" required="" name="name" value="{{Auth::user()->name}}">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputLastName">Last Name</label>
                  <input type="text" class="form-control" id="lastname" placeholder="" required="" name="surname" value="{{Auth::user()->surname}}">
                </div>
              </div>
              <div class="form-group">
                <label for="inputAddress">Address</label>
                <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St" name="address" value="{{Auth::user()->address}}">
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputCity">City</label>
                  <input type="text" class="form-control" id="city" name="city" value="{{Auth::user()->city}}">
                </div>
                <div class="form-group col-md-6">
                  <label for="inputCounty">Country</label>
                  {{Form::select('country',App\Models\Country::pluck('name','id'),Auth::user()->countries_id,['id'=>'country','class'=>'form-control','placeholder'=>'Select your country.','required'=>'required'])}}
                </div>  
              </div>
              <div class="text-center">
                <button type="submit" class="btn btn-dark btn-lg">Update Profile</button>
              </div>
            </form>
            <br>
          </div><!-- col-10 div-white -->
        </div>
          
        <div class="row">
          <div class="col-12 col-md-10 div-white mb-4">
            <div class="w-100"></div>
            @include('frontend.partials._kyc_profile')
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-md-10 div-white" style="margin-bottom: 2em;" id="changePassword">
            <div class="row">
              <div class="col-md-12 mt-3">
                <h4>Change Password</h4>
              </div>
            </div>
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <form class="mt-3" method="POST" action="/profile/changePassword" id="change-password-form">
              {!! csrf_field() !!}
              <div class="form-group">
                <label for="old_password">Old Password</label>
                <input type="password" 
                  class="form-control" 
                  id="old_password"  
                  name="old_password" 
                  required="required"
                  placeholder="Enter Old Password"
                  minlength="6"
                >
                @if ($errors->has('old_password'))
                <span class="help-block">
                  <strong>{{ $errors->first('old_password') }}</strong>
                </span>
                @endif
              </div>
              <div class="form-group">
                <label for="new_password">New Password</label>
                <input type="password" 
                  class="form-control" 
                  id="new_password" 
                  name="new_password" 
                  required="required"
                  placeholder="Enter New Password"
                  minlength="6"
                >
                @if ($errors->has('new_password'))
                <span class="help-block">
                  <strong>{{ $errors->first('new_password') }}</strong>
                </span>
                @endif
              </div>
              <div class="form-group">
                <label for="password_confirmation">Confirm New Password</label>
                <input type="password" 
                  class="form-control" 
                  id="new_password_confirmation" 
                  name="new_password_confirmation" 
                  required="required"
                  data-parsley-equalto="#new_password"
                  data-parsley-equalto-message= "This value should be the same as Password value."
                  placeholder="Enter Confirm Password"
                  minlength="6"
                >
              </div>
              <div class="text-center">
                <button type="submit" class="btn btn-dark btn-lg">Change Password</button>
              </div>
            </form>
            <br>
          </div><!-- col-10 div-white -->
        </div>

@endsection


@section('style')
<style>
.parsley-errors-list li.parsley-required {
    padding: 10px;
    color: #f00;
}
</style>
@endsection
@section('script')
  <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> -->
  {!! HTML::script('js/parsley.js'); !!}
<script>
  /*$('.country').select2();*/
  $('#profile-form').parsley();
  $('#change-password-form').parsley();
  $('#kycForm').parsley();

  $('.edit-button').click(function(){
    $('#edit-wallet-form').toggle();
  });

  $('.custom-file-input').on('change', function() { 
    let fileName = $(this).val().split('\\').pop(); 
    //alert(fileName);
    $(this).next('.custom-file-label').addClass("selected").html(fileName); 
  });
  
  window.Parsley.addValidator('filemimetypes', {
    requirementType: 'string',
    validateString: function (value, requirement, parsleyInstance) {

        var file = parsleyInstance.$element[0].files;

        if (file.length == 0) {
            return true;
        }

        var allowedMimeTypes = requirement.replace(/\s/g, "").split(',');
        return allowedMimeTypes.indexOf(file[0].type) !== -1;

    },
    messages: {
        en: 'File mime type not allowed'
    }
});
</script>

@endsection


