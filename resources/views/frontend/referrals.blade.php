@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-6 col-sm-12 col-12 ml-4" style="margin-top: 1em; color: #000; font-weight: bold; font-size: 1.5em; padding-left: 5px;">Happy times for everyone</div>
</div>
<div class="row">
    <div class="col-12 col-md-10 div-white mb-5">
        <div class="row">
            <div class="col-12 col-md-5">
                <br>
                <p>For every person invited you will receive 3.5% of their deposit amount.</p>
                <br>
                <strong>Your referral link:</strong>
                <form class="form-inline">
                    <div class="input-group">
                        <input type="text" class="form-control" id="input-btc" value="{{url('/').'/?ref='.Auth::user()->code}}" placeholder="" style="width: 17em;">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fa fa-link" aria-hidden="true"></i></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12 col-md-7">
                <hr>
                <div class="row">
                    <div class="col-3 col-md-2">Clicks</div>
                    <div class="col-7 col-md-8">Number of times your URL has been clicked</div>
                    <div class="col-1 col-md-2">{{(Auth::user()->ref_count)?Auth::user()->ref_count:'0'}}</div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-3 col-md-2">Referrals</div>
                    <div class="col-7 col-md-8">People who have signed up using your link</div>
                    <div class="col-1 col-md-2">{{(Auth::user()->referrer->count())?Auth::user()->referrer->count():'0'}}</div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-2">To earn</div>
                    <div class="col-7">Amount that will be applied to your FIC <br>wallet.</div>
                    <div class="col-3 text-center">
                    @if(Auth::user()->commission->where('status',0)->count() > 0)
                        {{number_format(Auth::user()->commission->where('status',0)->sum('fic'),8,'.',',')}}
                    @else
                    0.00000000
                    @endif
                    <br>FIC</div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-2">Paid </div>
                    <div class="col-7">Amount that has already been applied to <br>your FIC wallet.</div>
                    <div class="col-3 text-center">
                    @if(Auth::user()->commission->where('status',1)->count() > 0)
                        {{number_format(Auth::user()->commission->where('status',1)->sum('fic'),8,'.',',')}}
                    @else
                    0.00000000
                    @endif    
                    <br>FIC</div>
                </div>
            </div>
            <!-- col-7 -->
        </div>
        <div class="row">
          <div class="col">
            <br><br>
            <h4>Your referrals :</h4>
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-2"><b>ID</b></div>
          <div class="col-6"><b>Email</b></div>
          <div class="col-4 text-center">Status</div>
          <br><br>
        </div>
        @if(Auth::user()->referrer->count() > 0)
        <div class="row mb-5">
            @php
                $i=1;
                $pivotStatus = ['Registered','Ordered','Earned'];
            @endphp
            @foreach(Auth::user()->referrer as $people)
                <div class="col-2 mb-2"><b>{{$i}}</b></div>
                <div class="col-6"><b>{{$people->email}}</b></div>
                <div class="col-4 text-center"><b>{{$pivotStatus[$people->pivot->status]}}</b></div>
                @php($i++)
            @endforeach
        </div>
        @endif
    </div>
    <!-- div-white content -->
    <div class="col"></div>
</div>
@endsection