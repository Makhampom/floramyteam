@extends('layouts.default')
@section('content')
<div class="row">
  <div class="col-md-6 col-sm-12 col-xs-12" style="margin-top: 1em; color: #000; font-weight: bold; font-size: 1.5em;">Your Setting here</div></div>  
    <div class="row">
      <div class="col-md-9 col-sm-12 col-12 div-white" style="margin-bottom: 2em;">
        <br>
        <h6>Two-Factor Authentication : <b>{{($user->google2fa_status == 1)?'Enabled':'Disabled'}}</b></h6>
        @if($user->google2fa_status == 1)
          <button type="button" 
                  class="btn btn-dark" 
                  role="button" 
                  aria-expanded="false" 
                  aria-controls="collapseExample"
                  id="enableTFA">Disable Two-Factor Authentication
          </button>
            {!! csrf_field() !!}
            <div class="collapse" id="show-2fa-form">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-12"> 
                    <div class="form-group">
                      <label for=""><b>Your Authentication Key :</b></label>
                      <input type="text" class="form-control" id="token" placeholder="" name="token">
                    </div>
                  </div>
                </div>
              <div class="row">
                <div class="col-12 col-sm-12 col-md-12 mt-3 mb-3 text-center">
                  <button type="submit" id="submit" class="btn btn-warning btn-lg" style="color: #fff;">Update</button>
                </div>
            </div>
            </div>
          </div>
        @endif
        @if($user->google2fa_status == 0)
        <button type="button" 
                class="btn btn-dark" 
                role="button" 
                aria-expanded="false" 
                aria-controls="collapseExample"
                id="enableTFA">Enable Two-Factor Authentication
        </button>
        <div class="mt-3"></div>

          {!! csrf_field() !!}
          <div class="collapse" id="show-2fa-form">
            <div class="row">
              <p class="mt-2 ml-3">Two-Factor Authentication :</p>
              <div class="w-100"></div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <img alt="Two-Factor Authentication token" id="TFA_key">
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <h6>Please write down your authentication key :</h6>  
                <p><b><span id = "TFA_key">Wait Ajax</span></b></p>
                <p>Do not save the QR code in any digital form. The QR code must be printed on paper and saved in a secure place, where only you can access it.</p>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6 col-sm-6 col-12"> 
                <div class="form-group">
                  <label for=""><b>Your Authentication Key :</b></label>
                  <input type="text" class="form-control" id="TFA_key" placeholder="" disabled>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-12"> 
                <div class="form-group">
                  <label for=""><b>Enter Your 6-Digit Authentication Code :</b></label>
                  <input type="text" class="form-control" maxlength="6" required="required" id="totp" placeholder="" name="totp">
                  @if ($errors->has('totp'))
                  <span class="help-block">
                      <strong>{{ $errors->first('totp') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
            </div>

            <div class="row" style="margin-bottom: 2em;">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <h6>IMPORTANT NOTICE</h6>
                <p>Always write down your authentication key! You will not be able to log in to your account in case you lose your mobile device. If you are having problems with our system rejecting your authentication code, check your mobile device's clock. It must be synchronized perfectly. Your device will generate a new code every 30 seconds.</p>
              </div>
            </div>

            <div class="row">
              <div class="col-12 col-sm-12 col-md-12 mt-3 mb-3 text-center">
                <button type="submit" id="submit" class="btn btn-warning btn-lg" style="color: #fff;">Update</button>
              </div>
            </div>

          </div>

        </div>
      @endif
@endsection
@section('script')
@php($script = ($user->google2fa_status==1)?'disable':'enable')
<script src="{{asset('js/pages/setting_'.$script.'_token.js')}}"></script>
<script>
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  function inputValidate(elem,text='Your code is not valid.'){
    $(elem).val('');
    $(elem).focus();
    $(elem).addClass('is-invalid');
  }
</script>
@endsection