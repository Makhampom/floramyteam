<div class="form-group col-sm-12 mt-3">
    <h5>Know Your Customer : 
    @if(Auth::user()->kyc_file != '')
        <span class="{{App\Models\User::$kycStatus[Auth::user()->kyc_status]['style']}}">{!!Auth::user()->getKycStatusDisplay(0)!!}</span>
    @else
    <span class="text-danger">Please Upload your KYC file.</span>
    @endif
    </h5>
</div>
@if(Auth::user()->kyc_status != 1)
<div class="form-group col-sm-12 mt-3">
    @if (session('kyc_error'))
        <div class="alert alert-danger">
            {{ session('kyc_error') }}
        </div>
    @endif
    @if (session('kyc_success'))
        <div class="alert alert-success">
            {{ session('kyc_success') }}
        </div>
    @endif
    <form name="kycForm" id="kycForm" action="/profile/kycUpload" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
        @if(Auth::user()->kyc_file)
        @php($file = asset('images/kyc/'.Auth::user()->kyc_file))
        <div class="col-12 col-md-10 mt-2 mb-2">
            @if(File::extension($file) == 'pdf')
            <object data="{{asset('images/kyc/'.Auth::user()->kyc_file)}}" type="application/pdf" height="100%" width="300px"></object>
            @else
            <img class="col-md-6 col-lg-6" src="{{asset('images/kyc/'.Auth::user()->kyc_file)}}">
            @endif
        </div>
        <div class="w-100"></div>
        <div class="col-12 col-md-12"> 
            <label for="FormControlFile1">Change file : upload you IP Card/Passport (JPEG/JPG,PNG,PDF)*</label>
            <div class="row">
                <div class="custom-file col-sm-3 col-md-6 col-lg-6 ml-3">
                    <input  type="file" 
                            name="kyc_file" 
                            id="kyc_file" 
                            required="required" 
                            class="custom-file-input"
                            data-parsley-trigger="change" 
                            data-parsley-filemimetypes="image/jpeg,image/png,application/pdf"
                    >
                    <label class="custom-file-label" for="customFile"></label>
                    @if ($errors->has('kyc_file'))
                    <span class="help-block">
                        <strong>{{ $errors->first('kyc_file') }}</strong>
                    </span>
                    @endif
                </div>
                <input type="submit" class="btn btn-dark col-sm-2 ml-3" value="upload">
            </div>
        </div>
        @else
        <div class="col-12 col-md-12">
            <label for="FormControlFile1">upload you IP Card/Passport (JPEG/JPG,PNG,PDF)*</label>
            <div class="row">
                <div class="custom-file col-sm-3 col-md-6 col-lg-6 ml-3">
                    <input  type="file" 
                            name="kyc_file" 
                            id="kyc_file" 
                            required="required" 
                            class="custom-file-input"
                            data-parsley-trigger="change" 
                            data-parsley-filemimetypes="image/jpeg,image/png,application/pdf"
                    >
                    <label class="custom-file-label" for="customFile"></label>
                    @if ($errors->has('kyc_file'))
                    <span class="help-block">
                        <strong>{{ $errors->first('kyc_file') }}</strong>
                    </span>
                    @endif
                </div>
                <input type="submit" class="btn btn-dark col-sm-2 ml-3" value="upload">
            </div>
        </div>
        @endif
    </form>
</div>
@else
<div class="col-12 col-md-12 mb-4">
    <div class="row">
        @php($file = asset('images/kyc/'.Auth::user()->kyc_file))
        <div class="col-12 col-md-10 mt-2 mb-2">
            @if(File::extension($file) == 'pdf')
            <object data="{{asset('images/kyc/'.Auth::user()->kyc_file)}}" type="application/pdf" height="100%" width="300px"></object>
            @else
            <img class="col-md-6 col-lg-6" src="{{asset('images/kyc/'.Auth::user()->kyc_file)}}">
            @endif
        </div>
    </div>
</div>
@endif