<tr>
    <th scope="row">{{$order}}</th>
    <td>{{$data->created_at->format('d-m-Y H:i:s')}}</td>
    <td>
        @if($data->getTransactionStatusView() == "Pending")
        <span class="badge badge-pill badge-warning">{{ $data->getTransactionStatusView() }}</span>
        @endif
        @if($data->getTransactionStatusView() == "Approved")
        <span class="badge badge-pill badge-success">{{ $data->getTransactionStatusView() }}</span>
        @endif
        @if($data->getTransactionStatusView() == "Rejected")
        <span class="badge badge-pill badge-danger">{{ $data->getTransactionStatusView() }}</span>
        @endif
    </td>
    <td>{{$data->to_wallet}}</td>
    <td>{{$data->transfer_code}}</td>
    <td>{{$data->wallet->code}}</td>
    <td>{{($data->actual_send != '')?decimalMoneyFormat($data->actual_send):decimalMoneyFormat($data->send_value)}}</td>
    <td>{{($data->actual_fic  != '')?decimalMoneyFormat($data->actual_fic):decimalMoneyFormat($data->fic_receive)}}</td>
    <td>{{($data->actual_bonus  != '')?decimalMoneyFormat($data->actual_bonus):decimalMoneyFormat($data->fic_bonus)}}</td>
    <td>{{($data->actual_fic_recieve  != '')?$data->actual_fic_recieve:$data->ttl_receive}}</td>
</tr>