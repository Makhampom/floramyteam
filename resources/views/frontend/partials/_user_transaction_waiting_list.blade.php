<tr>
    <td scope="row">{{$order}}</td>
    <td>{{$data->created_at->format('d-m-Y H:i:s')}}</td>
    <td>{{$data->to_wallet}}</td>
    <td class="table-danger">
        <input class="cus-radius" type="text" name="send_transaction" placeholder = "">
    </td>            
    <td><input class="btn btn-warning" type="submit" name="confirm_transaction" data-id="{{$data->id}}" value="Send Confirm"></td>
    <td>{{$data->wallet->code}}</td>
    <td>{{($data->actual_send != '')?decimalMoneyFormat($data->actual_send):decimalMoneyFormat($data->send_value)}}</td>
    <td>{{($data->actual_fic  != '')?decimalMoneyFormat($data->actual_fic):decimalMoneyFormat($data->fic_receive)}}</td>
    <td>{{($data->actual_bonus  != '')?decimalMoneyFormat($data->actual_bonus):decimalMoneyFormat($data->fic_bonus)}}</td>
    <td>{{($data->actual_fic_recieve  != '')?$data->actual_fic_recieve:$data->ttl_receive}}</td>
</tr>