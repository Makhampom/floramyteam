@extends('layouts.default')
@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 col-12" style="margin-top: 1em; margin-left: 1em; color: #000; font-weight: bold; font-size: 1.5em; padding-left: 5px;">Your order & transaction here</div>
</div>  

<div class="row">

    <div class="div-white col-md-11 col-12 table-responsive" style="margin-bottom: 2em;">
      <br>
      @php($datas = App\Models\Transaction::where('user_id',Auth::user()->id)->whereNull('transfer_code')->orderBy('updated_at','desc')->get())
      @if($datas->isNotEmpty())
      @php($i = 1)
      <div class="mt-5">
        <h5>Confirm Transection</h5>
      </div>
      <table class="table table-bordered table-hover">
        <thead class="text-center bg-white">
          <tr>
            <th>No.</th>
            <th>Date</th>
            <th>Send To Address</th>
            <th><strong>Transaction ID</strong></th>
            <th>Payment Confirm</th>
            <th>Type</th>
            <th>Send</th>
            <th>FIC Receive</th>
            <th>FIC Bonuns</th>
            <th>TTL Receive</th>
          </tr>
        </thead>
        <tbody class="bg-light text-dark">
          @foreach($datas as $key => $data)
            @include('frontend.partials._user_transaction_waiting_list', ['data'=>$data,'order'=>$i])
            @php($i++)
          @endforeach
        </tbody>
      </table>
      @endif

      <!-- <hr class="col mt-5"> -->
      @php($datas = App\Models\Transaction::where('user_id',Auth::user()->id)->whereNotNull('transfer_code')->orderBy('updated_at','desc')->get())
      @if($datas->isNotEmpty())
      @php($i = 1)
      <div class="mt-5">
        <h5>Transection History</h5>
      </div>
      <table class="table table-bordered table-hover">
        <thead class="text-center bg-white">
          <tr>
            <th>No.</th>
            <th>Date</th>
            <th>Status</th>
            <th>Send To Address</th>
            <th><strong>Transaction ID</strong></th>
            <th>Type</th>
            <th>Send</th>
            <th>FIC Receive</th>
            <th>FIC Bonuns</th>
            <th>TTL Receive</th>
          </tr>
        </thead>
        <tbody class="bg-light text-dark">
          @foreach($datas as $key => $data)
            @include('frontend.partials._user_transaction_list', ['data'=>$data,'order'=>$i])
            @php($i++)
          @endforeach
        </tbody>
      </table>
      @endif
    </div>
</div>
@endsection
@section('style')
<style>
.parsley-errors-list li {
    padding: 10px;
    color: #f00;
}
</style>
@endsection
@section('script')
<script>
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $(document).ready(function() {
    $('input[name="confirm_transaction"]').click(function(){
      var id = $(this).data('id');
      var transaction = $(this).parents('tr').find('input[name="send_transaction"]');
      var token = transaction.val();
      $(this).prop('disabled',true);
      transaction.prop('disabled',true);
      if(token != '' && id != ''){
        $.ajax({
          url: "/order/confirmTransacrion",
          method: "POST",
          data:{id:id,token:token}
        }).done(function( response ) {
          if(response.status=='error'){
            alert("Something went wrong, please try agian.");
            $(this).prop('disabled',false);
            transaction.prop('disabled',false);
          }else{
            location.reload();   
          }
        }).fail(function( jqXHR, textStatus ) {
          alert( "Request failed: " + textStatus );
          location.reload(); 
        });
      }else{
        $(this).prop('disabled',false);
        transaction.prop('disabled',false);
      }
    });
  });
</script>
@endsection



