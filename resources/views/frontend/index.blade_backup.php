@extends('layouts.default')
@section('content')
<div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12" style="margin-left: 1em; margin-top: 1em; color: #000; font-weight: bold; font-size: 1.5em; padding-left: 5px;">Calculate your investment amount</div>
      </div>  

      <div class="row">
        <div class="col-md-7 col-sm-7 col-xs-12 div-white" style="margin-bottom: 2em;">
          <!-- Example Bar Chart Card-->
          <div class="col-md-12" style="margin-top: 1em; margin-bottom: 1em; color: #000;">
            Buy FIC coins with BTC or ETH
          </div>
          <ul>@each('frontend.partials._product_list', $list, 'product')</ul>
            {{--<a href="buy-with-eth.php" class="col select-currency">
              <div>
                <img src="{{asset('images/buy-with-eth.png')}}" class="img-hover">
                <div class="top-em1">Buy with ETH</div>
              </div>
            </a>
            <a href="buy-with-bch.php" class="col select-currency">
              <div>
                <img src="{{asset('images/buy-with-bch.png')}}" class="img-hover">
                <div class="top-em1">Buy with BCH</div>
              </div>
            </a>
            <a href="buy-with-dash.php" class="col select-currency">
              <div>
                <img src="{{asset('images/buy-with-dash.png')}}" class="img-hover">
                <div class="top-em1">Buy with DASH</div>
              </div>
            </a>
            <a href="buy-with-eos.php" class="col col-xs-6 select-currency">
              <div>
                <img src="{{asset('images/buy-with-eos.png')}}" class="img-hover">
                <div class="top-em1">Buy with EOS</div>
              </div>
            </a>
            <a href="buy-with-ltc.php" class="col col-xs-6 select-currency">
              <div>
                <img src="{{asset('images/buy-with-ltc.png')}}" class="img-hover">
                <div class="top-em1">Buy with LTC</div>
              </div>
            </a>
            <a href="buy-with-omg.php" class="col col-xs-6 select-currency">
              <div>
                <img src="{{asset('images/buy-with-omg.png')}}" class="img-hover">
                <div class="top-em1">Buy with OMG</div>
              </div>
            </a>
            <a href="buy-with-salt.php" class="col col-xs-6 select-currency">
              <div>
                <img src="{{asset('images/buy-with-salt.png')}}" class="img-hover">
                <div class="top-em1">Buy with SALT</div>
              </div>
            </a>--}}

          <!-- <span class="col-md-2 select-currency" style="width: 112px; height: 176px; background-color: #ccc;">
            
          </span>
          <span class="col-md-2">sf</span>
          <span class="col-md-2">sdaf</span> -->
          <div class="row" style="margin-bottom: 1em;"></div>
        </div>
        <div class="col-md-4 div-white" style="margin-bottom: 2em;">
          <div class="col-md-12" style="margin-top: 1em; margin-bottom: 1em; color: #000;">
            5 Steps Guide
          </div>
          <div class="col-md-12 text-16">1. Calculate how much coins you want to buy</div>
          <div class="col-md-12 text-16 top-em1">2. Copy/Scan the displayed address</div>
          <div class="col-md-12 text-16 top-em1">3. Send your cryptocurrency from any wallet you like (also exchange wallet is ok)</div>
          <div class="col-md-12 text-16 top-em1">4. After the payment click SUBMIT ORDER and we keep you posted with status e-mails, so check your mails</div>
          <div class="col-md-12 text-16 top-em1">5. Coins will be send to your FloraFIC wallet. <span style="color: #f00;">Coins can take up to 3 weeks after the ICO to appear in your wallet, but usually appear within 48 hours.</span></div>
          <div class="top-em1"></div>
        </div>
        
      </div>
      @endsection
      @section('style')
      <link href="{{asset('css/index-custom.css')}}" rel="stylesheet" type="text/css">
      @endsection