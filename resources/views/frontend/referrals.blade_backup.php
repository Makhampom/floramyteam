@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-6 col-sm-12 col-12" style="margin-top: 1em; color: #000; font-weight: bold; font-size: 1.5em; padding-left: 5px;">Happy times for everyone</div>
</div>
<div class="row">
    <div class="col-10 col-xs-12 div-white">
        <div class="row">
            <div class="col">
                <br>
                <p>For every person invited you will receive 3.5% of their deposit amount. The referral will receive a 1.5% bonus using your invite URL.</p>
                <br>
                <strong>Your referral link:</strong>
                <form class="form-inline">
                    <div class="input-group">
                        <input type="text" class="form-control" id="input-btc" value="{{url('/').'/?ref='.Auth::user()->code}}" placeholder="" style="width: 18em;">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fa fa-link" aria-hidden="true"></i></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-7 col-xs-12">
                <hr>
                <div class="row">
                    <div class="col">Clicks</div>
                    <div class="col-8">Number of times your URL has been clicked</div>
                    <div class="col">{{(Auth::user()->ref_count)?Auth::user()->ref_count:'0'}}</div>
                </div>
                <hr>
                <div class="row">
                    <div class="col">Referrals</div>
                    <div class="col-8">People who have signed up using your link</div>
                    <div class="col">{{(Auth::user()->referrer->count())?Auth::user()->referrer->count():'0'}}</div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-2">To earn</div>
                    <div class="col-7">Amount that will be applied to your TRF <br>wallet.</div>
                    <div class="col-3 text-center">0.00000000 <br>TRF</div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-2">Paid </div>
                    <div class="col-7">Amount that has already been applied to <br>your TRF wallet.</div>
                    <div class="col-3 text-center">0.00000000 <br>TRF</div>
                </div>
            </div>
            <!-- col-7 -->
        </div>
        @if(Auth::user()->referrer->count() > 0)
        <div class="row">
            <div class="col">
                <br><br>
                <h4>Your referrals :</h4>
                <hr>
            </div>
        </div>
        <div class="row">
            @php
                $i=1;
                $pivotStatus = ['Registered','Ordered','Earned'];
            @endphp
            @foreach(Auth::user()->referrer as $people)
                <div class="col-2"><b>{{$i}}</b></div>
                <div class="col-2"><b>{{$people->email}}</b></div>
                <div class="col text-right"><b>{{$pivotStatus[$people->pivot->status]}}</b></div>
                <br><br>
                @php($i++)
            @endforeach
        </div>
        @endif
    </div>
    <!-- div-white content -->
    <div class="col"></div>
</div>
@endsection