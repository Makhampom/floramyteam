@extends('layouts.default')
@section('content')
      <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12" style="margin-left: 1em; margin-top: 1em; color: #000; font-weight: bold; font-size: 1.5em; padding-left: 5px;">Calculate your investment amount</div>
      </div>  

      <div class="row">
        <div class="col-12 col-md-7 col-sm-7 div-white" style="margin-bottom: 2em;">
          <!-- Example Bar Chart Card-->
          <div class="col-12 col-md-12" style="margin-top: 1em; margin-bottom: 1em; color: #000; padding-left: 0px;">
            BUY FIC TOKENS
          </div>

          <div class="row text-center">
            <div class="col-6 col-md-3">
              <div class="col-12 col-md-12 select-currency">
                <a href="{{URL::to('/order/btc')}}" class="">
                  <div>
                    <img src="{{asset('images/buy-with-btc.png')}}" class="img-hover">
                    <div class="top-em1">Buy with BTC</div>
                  </div>
                </a>
              </div>
            </div>
            <div class="col-6 col-md-3">
              <div class="col-12 col-md-12 select-currency">
                <a href="{{URL::to('/order/eth')}}" class="">
                <div>
                  <img src="{{asset('images/buy-with-eth.png')}}" class="img-hover">
                  <div class="top-em1">Buy with ETH</div>
                </div>
              </a>
              </div>
            </div>
            <div class="col-6 col-md-3 mgtop1">
              <div class="col-12 col-md-12 select-currency">
                <a href="{{URL::to('/order/bch')}}">
                <div>
                  <img src="{{asset('images/buy-with-bch.png')}}" class="img-hover">
                  <div class="top-em1">Buy with BCH</div>
                </div>
              </a>
              </div>
            </div>
            <div class="col-6 col-md-3 mgtop1">
              <div class="col-12 col-md-12 select-currency">
                <a href="{{URL::to('/order/dash')}}">
                  <div>
                    <img src="{{asset('images/buy-with-dash.png')}}" class="img-hover">
                    <div class="top-em1">Buy with DASH</div>
                  </div>
                </a>
              </div>
            </div>
          </div>             
          <br>
          <div class="row text-center">
            <div class="col-6 col-md-3">
              <div class="col-12 col-md-12 select-currency">
                <a href="{{URL::to('/order/eos')}}">
                  <div>
                    <img src="{{asset('images/buy-with-eos.png')}}" class="img-hover">
                    <div class="top-em1">Buy with EOS</div>
                  </div>
                </a>
              </div>
            </div>
            <div class="col-6 col-md-3">
              <div class="col-12 col-md-12 select-currency">
                <a href="{{URL::to('/order/ltc')}}">
                  <div>
                    <img src="{{asset('images/buy-with-ltc.png')}}" class="img-hover">
                    <div class="top-em1">Buy with LTC</div>
                  </div>
                </a>
              </div>
            </div>
            <div class="col-6 col-md-3 mgtop1">
              <div class="col-12 col-md-12 select-currency">
                <a href="{{URL::to('/order/omg')}}">
                  <div>
                    <img src="{{asset('images/buy-with-omg.png')}}" class="img-hover">
                    <div class="top-em1">Buy with OMG</div>
                  </div>
                </a>
              </div>
            </div>
            <div class="col-6 col-md-3 mgtop1">
              <div class="col-12 col-md-12 select-currency">
                <a href="{{URL::to('/order/salt')}}">
                  <div>
                    <img src="{{asset('images/buy-with-salt.png')}}" class="img-hover">
                    <div class="top-em1">Buy with SALT</div>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <!-- <span class="col-md-2 select-currency" style="width: 112px; height: 176px; background-color: #ccc;">
            
          </span>
          <span class="col-md-2">sf</span>
          <span class="col-md-2">sdaf</span> -->
        <div class="row" style="margin-bottom: 1em;"></div>
        </div>
        <div class="col-md-4 div-white" style="margin-bottom: 2em;">
          <div class="col-md-12" style="margin-top: 1em; margin-bottom: 1em; color: #000;">
            5 Steps Guide
          </div>
          <div class="col-md-12 text-16">1. Calculate how much coins you want to buy</div>
          <div class="col-md-12 text-16 top-em1">2. Copy/Scan the displayed address</div>
          <div class="col-md-12 text-16 top-em1">3. Send your cryptocurrency from any wallet you like (also exchange wallet is ok)</div>
          <div class="col-md-12 text-16 top-em1">4. After the payment click SUBMIT ORDER and we keep you posted with status e-mails, so check your mails</div>
          <div class="col-md-12 text-16 top-em1">5. Coins will be send to your FloraFIC wallet. 
            <span style="color: #f00;">Due to Pre ICO duration, you will receive FIC token in the ICO day.</span></div>
          <div class="top-em1"></div>
        </div>


      @endsection

      @section('style')
      <link href="{{asset('css/index-custom.css')}}" rel="stylesheet" type="text/css">
      @endsection

