@extends('layouts.plain')
@extends('layouts.mail_contact');
@section('style')
    <link rel="stylesheet" href="{{asset('vendor/animate/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/canvan_animate/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/css_custom.css')}}">
@endsection

@section('content')
<div class="card card-login mx-auto mb-5" style="margin-top: -1em;">
  <div class="card-body col-12 col-md-12" style="padding: 0;">
    <div class="col-12 col-md-12">
      <div class="row">
        <div class=" col-12 text-center" style="padding: 0px 0px 0px 0px;">
          <div style="color: #0099cc;">
            <div style="font-weight: bold;" class="">
              <div style="height: 0.7em;"></div>
              <p> RESET YOUR PASSWORD</p>
            </div>
          </div>
        </div>
      </div>
    </div>  

    <div class="col-12 col-md-12 mt-3">
        <form action="{{URL::to('/password/email')}}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-12">
                        <label for="exampleInputEmail1">YOUR EMAIL</label> &nbsp;<span id="chk_email"></span>
                        <input type="email" class="form-control" name="email" required="required" placeholder="ENTER EMAIL">
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <input type="hidden" name="email_hidden" value="1">
                    </div>
                </div>
            </div>
            
            <div class="col-12 col-md-12 text-center">
                <input type="submit" id="reset_submit" value="RESET" class="btn btn-dark btn-lg">
            </div>

        </form>
        <div class="col-12 col-md-12">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center">
                    <a class="d-block small" href="{{URL::to('/member/login')}}">Login Page</a>
                </div>       
                <div class="col-md-4"></div>
            </div>
        </div>
        <br>
    </div>
  </div>
</div>


@endsection


@section('script')
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#reset_submit').attr({
            disabled: 'disabled'
        });

        $('input[name=email]').keyup(function(event) {
            var email = $('input[name=email]').val();
            //var user = $('input[name=user]').val();
            
            //$('#chk_email').html(email);
            //$('#chk_user').html(user);

            var type = "email";

            $.post('/api/chkExistingEmail', {type:type, email: email}, function(data) {

                if (data.stt !== 'success') {
                    $('#chk_email').html('<span class="red-txt">Does not exist</span>');
                    $('#reset_submit').attr({disabled:'disabled'});
                    $('input[name=email_hidden]').attr({value:'1'});
                }else if(data.stt === 'success'){
                    $('#chk_email').html('<span class="green-txt">Existing email</span>');
                    $('#reset_submit').removeAttr("disabled");
                    $('input[name=email_hidden]').attr({value:'0'});
                }

            },"json");
        });


    });
</script>

<script src="{{asset('js/animate/particles.min.js')}}"></script>
<script src="{{asset('js/animate/particles_spider.js')}}"></script>

@endsection

