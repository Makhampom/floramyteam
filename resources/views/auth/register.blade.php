@extends('layouts.plain')
@extends('layouts.mail_contact');
@section('content')
<div class="card-body col-12 col-md-12" style="padding: 0;">
    <div class="col-12 col-md-12">
      <div class="row" style="padding: 0px 0px 0px 0px;">
        <div class="col-6 text-center" style="padding: 0px 0px 0px 0px;">
          <div style="color: #fff;background-color: #0099cc;height: 3em;">
            <div style="font-weight: bold;">
              <div style="height: 0.7em;"></div>
              <p>LOGIN</p>
            </div>
          </div>
        </div>
        <div class="col-6 text-center" style="padding: 0px 0px 0px 0px;">
          <div style="color: #0099cc;">
            <div style="font-weight: bold;">
              <div style="height: 0.7em;"></div>
              <p>RESGISTER</p>
            </div>
          </div>
        </div>   
      </div>
    </div>  

    <form action="{{URL::to('/member/register')}}" method="post">
        {{ csrf_field() }}
        <div class="row mt-3">
            <div class="col-12 col-md-12">
                <div class="col-12 col-md-12">
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="exampleInputName">FIRST NAME</label>
                                <input class="form-control" name="name" required="required" id="exampleInputName" type="text" aria-describedby="nameHelp" placeholder="Enter Firstname">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div> 
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="exampleInputLastName">LAST NAME</label>
                                <input class="form-control" name="lastname" id="exampleInputLastName" type="text" aria-describedby="nameHelp" placeholder="Enter Lastname">
                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="exampleFormControlSelect1">COUNTRY</label>
                                {{Form::select('country',App\Models\Country::pluck('name','id'),Auth::user()->countries_id,['id'=>'country','class'=>'form-control','placeholder'=>'Select your country.','required'=>'required'])}}
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="eth_wallet">ETH Wallet </label>
                                <input class="form-control" name="eth_wallet" id="eth_wallet" type="text" placeholder="Enter ETH Wallet ">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">EMAIL ADDRESS<span class="red-txt">*</span></label>
                        <input type="email" class="form-control" name="email" required="required" placeholder="Enter Email">
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <span id="chk_email"></span>
                        <input type="hidden" name="email_hidden" value="1">
                    </div>
                    <div class="form-group">
                        <label for="email_confirm">EMAIL CONFIRM<span class="red-txt">*</span></label>
                        <input type="email" class="form-control" name="email_confirm" required="required" placeholder="Enter Confirm Email"> 
                        <span id="chk_email_confirm"></span>         
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="exampleInputPassword1">PASSWORD
                                    <span class="red-txt">*</span>
                                    <span id="must6digit" class="red-txt">password must be 6 digit</span>
                                </label>
                                <input class="form-control" name="password" required="required" id="exampleInputPassword1" type="password" placeholder="Enter Password">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-12">
                                <label for="exampleInputPassword1">PASSWORD CONFIRM
                                    <span class="red-txt">*</span>
                                </label>
                                <input class="form-control" name="password_confirm" required="required" id="password_confirm" type="password" placeholder="Enter Confirm Password">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 text-center">
                        <div class="form-check"> 
                          <input class="form-check-input" type="checkbox" value="" id="agreement">
                          <label class="form-check-label" for="agreement">
                            Agreement
                          </label>
                          <a href="#">Readme</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 mt-4 text-center">
            <input type="submit" id="" value="REGISTER" class="btn btn-primary" style="width: 80%;background-color: #001f2c;">
        </div>  
    </form>
    <div class="text-center">
        <a class="d-block small mt-3" href="{{URL::to('/member/login')}}">Login Page</a>
        <a class="d-block small" href="{{URL::to('password/reset')}}">Forgot Password?</a>
        <br>
    </div>
</div>

@endsection

@section('style')
<link rel="stylesheet" href="{{asset('css/css_custom.css')}}">
@endsection

@section('script')
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#regis_submit').attr({
            disabled: 'disabled'
        });

        $('input[name=email]').keyup(function(event) {
            var email = $('input[name=email]').val();
            //var user = $('input[name=user]').val();
            
            //$('#chk_email').html(email);
            //$('#chk_user').html(user);

            var type = "email";

            $.post('/api/checkUserEmail', {type:type, email: email}, function(data) {

                if (data.stt !== 'success') {
                    $('#chk_email').html('<span class="red-txt">Email in used</span>');
                    $('#regis_submit').attr({disabled:'disabled'});
                    $('input[name=email_hidden]').attr({value:'1'});
                }else if(data.stt === 'success'){
                    $('#chk_email').html('<span class="green-txt">Email available</span>');
                    $('#regis_submit').removeAttr("disabled");
                    $('input[name=email_hidden]').attr({value:'0'});
                }

            },"json");

            /*var email_hidden = $('input[name=email_hidden').val();

            console.log(email_hidden);

            if (email_hidden == 1) {
                $('#regis_submit').attr({disabled:'disabled'});
            }

            if (email_hidden == 0) {
                $('#regis_submit').removeAttr("disabled");
            }*/


        });


    });
</script>
@endsection

