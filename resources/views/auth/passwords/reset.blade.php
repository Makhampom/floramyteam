@extends('layouts.plain')
@extends('layouts.mail_contact');
@section('content')
<div class="container">

    <div class="card card-login mx-auto mb-5" style="margin-top: -1em;">
      <div class="card-body col-12 col-md-12" style="padding: 0;">
        <div class="col-12 col-md-12">
          <div class="row">
            <div class=" col-12 text-center" style="padding: 0px 0px 0px 0px;">
              <div style="color: #0099cc;">
                <div style="font-weight: bold;" class="">
                  <div style="height: 0.7em;"></div>
                  <p> Reset Password</p>
                </div>
              </div>
            </div>
          </div>
        </div>  
        <div class="col-12 col-md-12 mt-3">
            <form class="form-horizontal" method="POST" action="{{URL::to('/password/reset')}}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-12 control-label">E-Mail Address</label>
                    <div class="col-md-12">
                        <input id="email_show" type="email" class="form-control" name="email_show" value="{{ $email or old('email') }}" {{($email != '' || old('email') != '')?'disabled':''}} required>
                        <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required style="display:none">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-12 control-label">Password</label>

                    <div class="col-md-12">
                        <input id="password" type="password" class="form-control" name="password" required  minlength="6" autofocus >
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm" class="col-md-12 control-label">Confirm Password</label>
                    <div class="col-md-12">
                        <input id="password_confirmation" 
                        type="password" 
                        class="form-control" 
                        name="password_confirmation" 
                        required
                        data-parsley-equalto="#password"
                        data-parsley-equalto-message="This value should be the same as Password value."
                        minlength="6"
                        >

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 col-md-offset-4 text-center">
                        <button type="submit" class="btn btn-dark">
                            Reset Password
                        </button>
                    </div>
                </div>
            </form>

            <div class="col-12 col-md-12">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 text-center">
                        <a class="d-block small" href="{{URL::to('/member/login')}}">Login Page</a>
                    </div>       
                    <div class="col-md-4"></div>
                </div>
            </div>
            <br>
        </div>
      </div>
    </div>
    
</div>
<!-- end contain -->

@endsection
@section('script')
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="{{asset('js/animate/particles.min.js')}}"></script>
<script src="{{asset('js/animate/particles_spider.js')}}"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
{!! HTML::script('js/plugins/Parsley.js/dist/parsley.js'); !!}
<script>
    $(document).ready(function() {

      $('.register_call').click(function(event) {
        $('#login_box').hide('slow/400/fast', function() {
          $('#register_box').show('slow/400/fast', function() {
            
          });
        });
      });

      $('.login_call').click(function(event) {
        $('#register_box').hide('slow/400/fast', function() {
          $('#login_box').show('slow/400/fast', function() {
            
          });
        });
      });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#regis_submit').attr({
            disabled: 'disabled'
        });

        $('input[name=email]').keyup(function(event) {
            var email = $('input[name=email]').val();
            //var user = $('input[name=user]').val();
            
            //$('#chk_email').html(email);
            //$('#chk_user').html(user);

            var type = "email";

            $.post('/api/checkUserEmail', {type:type, email: email}, function(data) {

                if (data.stt !== 'success') {
                    $('#chk_email').html('<span class="red-txt">Email in used</span>');
                    $('#regis_submit').attr({disabled:'disabled'});
                    $('input[name=email_hidden]').attr({value:'1'});
                }else if(data.stt === 'success'){
                    $('#chk_email').html('<span class="green-txt">Email available</span>');
                    $('#regis_submit').removeAttr("disabled");
                    $('input[name=email_hidden]').attr({value:'0'});
                }

            },"json");

            /*var email_hidden = $('input[name=email_hidden').val();

            console.log(email_hidden);

            if (email_hidden == 1) {
                $('#regis_submit').attr({disabled:'disabled'});
            }

            if (email_hidden == 0) {
                $('#regis_submit').removeAttr("disabled");
            }*/


        });


    });
    $('select#country').select2();
    $('form').parsley();
</script>

@endsection
