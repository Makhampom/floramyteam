@extends('layouts.plain')
@extends('layouts.mail_contact');
@section('content')
<div class="card-header"><h5><strong>LOGIN</strong></h5></div>
<div class="card-body text-center">
  <form action="{{$location}}" method="post">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="exampleInputEmail1"><h5>EMAIL ADDRESS</h5></label>
      <input class="form-control bg-light text-center" id="exampleInputEmail1" name="email" type="email" aria-describedby="emailHelp" placeholder="Enter email" style="height: 3em;">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1"><h5>PASSWORD</h5></label>
      <input class="form-control bg-light text-center" id="exampleInputPassword1" name="password" type="password" placeholder="Password" style="height: 3em;">
    </div>
    <div class="form-group">
      <div class="form-check">
        <label class="form-check-label">
          <input class="form-check-input" type="checkbox"> Remember Password</label>
      </div>
    </div>
    <div class="form-group">
      <div class="form-check">
        <div class="g-recaptcha" data-sitekey="6Ldj-kIUAAAAAMxhXLY5C8OzsTKEg4iLVPyZ5jYl"></div>
        <!-- 6Ldj-kIUAAAAAMxhXLY5C8OzsTKEg4iLVPyZ5jYl -->
      </div>
    </div>
    <div class="col-12 col-md-12">
      <input type="submit" class="btn btn-primary btn-lg" value="LOGIN" style="border-radius: 30px;width: 60%;">
    </div>
  </form>
  <div class="text-center">
    @if($guard == 'user')      
    <a class="d-block small mt-3" href="{{URL::to('/member/register')}}">Register an Account</a>
    @endif
    <a class="d-block small" href="{{URL::to('password/reset')}}">Forgot Password?</a>
  </div>
</div>
@endsection
@section('script')
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection