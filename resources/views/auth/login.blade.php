@extends('layouts.plain')
@extends('layouts.mail_contact');
@section('style')
    <link rel="stylesheet" href="{{asset('vendor/animate/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/canvan_animate/style.css')}}">
    <style type="text/css">
      .login_call{
        cursor: pointer;
      }
      .register_call{
        cursor: pointer;
      }
      .login_call:hover{
        color: #fff;
        background-color: #00cccc;
      }
      .register_call:hover{
        color: #ffffff;
        background-color: #00cccc;
      }

      .red-txt{
        color: red;
      }

    </style>
@endsection

@section('content')
@if ($errors->has('register'))
<div id="login_box" class="card card-login mx-auto mb-5" style="margin-top: -1em;display:none">
@else
<div id="login_box" class="card card-login mx-auto mb-5" style="margin-top: -1em;">
@endif
  <div class="card-body col-12 col-md-12" style="padding: 0;">
    <div class="col-12 col-md-12">
      <div class="row">
        <div class="login_call col-6 text-center" style="padding: 0px 0px 0px 0px;">
          <div style="color: #0099cc;">
            <div style="font-weight: bold;" class="">
              <div style="height: 0.7em;"></div>
              <p> SIGN IN</p>
            </div>
          </div>
        </div>
        <div class="register_call col-6 text-center" style="padding: 0px 0px 0px 0px; border: 2px solid #0099cc;">
          <div style="color: #fff;background-color: #0099cc;height: 3em;">
            <div style="font-weight: bold;" class="">
              <div style="height: 0.7em;"></div>
              <p>RESGISTER</p>
            </div>
          </div>
        </div>   
      </div>
    </div>  

    <form action="{{$location}}" method="post"> 
      @if ($errors->has('error'))
        <span class="help-block">
            <strong>{{ $errors->first('error') }}</strong>
        </span>
        @endif 
      <div class="row mt-2 align-items-center">
        {{ csrf_field() }}
        <div class="col-12 col-md-12">
          <div class="col-12 col-md-12">
            <div class="form-group">
              <label for="exampleInputEmail1">Email ID</label>
              <input class="form-control bg-light" id="exampleInputEmail1" name="email" type="email" aria-describedby="emailHelp" placeholder="Enter email" style="height: 3em;">
              @if ($errors->has('email'))
              <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif
            </div>
          </div>  
          <div class="col-12 col-md-12">
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input class="form-control bg-light" id="exampleInputPassword1" name="password" type="password" placeholder="Password" style="height: 3em;">
              @if ($errors->has('password'))
              <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="col-12 col-md-12">
            <div class="form-group">
              <label for="exampleInputPassword1">2 Factor Authentication</label>
              <input type="text" class="form-control" maxlength="6" id="totp" placeholder="" name="totp">

              @if ($errors->has('totp'))
              <span class="help-block">
                  <strong>{{ $errors->first('totp') }}</strong>
              </span>
              @endif
            </div>
          </div>

          <div class="col-12 col-md-12 text-center">
            <div class="form-group">
              <div class="form-check text-center">
                <!-- <div class="g-recaptcha text-center" data-sitekey="6Ldj-kIUAAAAAMxhXLY5C8OzsTKEg4iLVPyZ5jYl"></div> -->
                <!-- 6Ldj-kIUAAAAAMxhXLY5C8OzsTKEg4iLVPyZ5jYl -->
              </div>
            </div>
          </div>



          <div class="col-12 col-md-12">
            <div class="row align-items-center">
              <div class="col-5 col-md-6">
                <input type="submit" class="btn btn-dark" value="LOGIN" style="width: 80%;background-color: #001f2c;">
              </div>
              <div class="col-7 col-md-6">
                <div class="form-group">
                  <div class="form-check">
                    <label class="form-check-label" style="font-size: 14px;">
                      <input class="form-check-input" type="checkbox">Remember Password</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <div class="col-12 col-md-12 mb-3">
      <div class="row align-items-center">
        
        <a class="col-md-4 d-block small text-left" href="{{URL::to('password/reset')}}">Forgot Password?</a>
      </div>
    </div>  

  </div>
</div>

@if ($errors->has('register'))
<div id="register_box" class="card card-login mx-auto mb-5" style="margin-top: -1em;">
@else
<div id="register_box" class="card card-login mx-auto mb-5" style="margin-top: -1em; display: none;">
@endif
  <div class="card-body col-12 col-md-12" style="padding: 0;">
      <div class="col-12 col-md-12">
        

        <div class="row">
          <div class="login_call col-6 text-center" style="padding: 0px 0px 0px 0px;">
            <div style="color: #fff;background-color: #0099cc; border: 2px solid #0099cc;">
              <div style="font-weight: bold;" class="">
                <div style="height: 0.7em;"></div>
                <p> SIGN IN</p>
              </div>
            </div>
          </div> 

          <div class="register_call col-6 text-center" style="padding: 0px 0px 0px 0px;">
            <div style="color: #0099cc;">
              <div style="font-weight: bold;" class="">
                <div style="height: 0.7em;"></div>
                <p>RESGISTER</p>
              </div>
            </div>
          </div>

        </div>


      </div>  

      <form action="{{URL::to('/member/register')}}" method="post" id="register-form">
        {{ csrf_field() }}
          <div class="row mt-3">
              <div class="col-12 col-md-12">
                  <div class="col-12 col-md-12">
                      <div class="form-group">
                          <div class="form-row">
                              <div class="col-md-12">
                                  <label for="exampleInputName">FIRST NAME <span class="red-txt">*</span></label>
                                  <input class="form-control" name="name" required="required" id="exampleInputName" type="text" aria-describedby="nameHelp" placeholder="Enter Firstname">
                                  @if ($errors->has('name'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('name') }}</strong>
                                      </span>
                                  @endif
                              </div> 
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="form-row">
                              <div class="col-md-12">
                                  <label for="exampleInputLastName">LAST NAME <span class="red-txt">*</span></label>
                                  <input class="form-control" name="lastname" id="exampleInputLastName"  required="required" type="text" aria-describedby="nameHelp" placeholder="Enter Lastname">
                                  @if ($errors->has('lastname'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('lastname') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="form-row">
                              <div class="col-md-12">
                                
                                  <label for="exampleFormControlSelect1">COUNTRY</label>
                                  {{Form::select('country',App\Models\Country::pluck('name','id'),null,['id'=>'country','class'=>'form-control','placeholder'=>'Select your country.'])}}
                                  <!-- @if ($errors->has('country'))
                                      <span class="help-block col-md-6">
                                          <strong>{{ $errors->first('country') }}</strong>
                                      </span>
                                  @endif -->
                              </div>
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="form-row">
                              <div class="col-md-12">
                                  <label for="eth_wallet">ETH Wallet </label>
                                  {{Form::text('eth_wallet',null,[
                                    'class'=>"form-control",
                                    'id'=>"eth_wallet",
                                    'placeholder'=>"Enter ETH Wallet" 
                                ])}}
                                  @if ($errors->has('eth_wallet'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('eth_wallet') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="exampleInputEmail1">EMAIL ADDRESS<span class="red-txt">*</span></label>
                          {{Form::email('email',null,[
                                'class'=>"form-control",
                                'required'=>'true',
                                'id'=>"register-email",
                                'placeholder'=>"Enter Email" 
                            ])}}
                        @if ($errors->has('email'))
                              <span class="help-block" id="chk_email">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                          <input type="hidden" name="email_hidden" value="1">
                      </div>
                        <div class="form-group">
                            <label for="email_confirmation">EMAIL CONFIRM<span class="red-txt">*</span></label>
                            {{Form::email('email_confirmation',null,[
                                'class'=>"form-control",
                                'required'=>'true',
                                'id'=>"register-email-confirmation",
                                'data-parsley-equalto'=>"#register-email",
                                'data-parsley-equalto-message'=>'This value should be the same as Email value.',
                                'placeholder'=>"Enter Confirm Email" 
                            ])}}
                          <span id="chk_email_confirm"></span>         
                      </div>

                      <div class="form-group">
                          <div class="form-row">
                              <div class="col-md-12">
                                  <label for="register-password">PASSWORD
                                      <span class="red-txt">*</span>
                                      <span id="must6digit" class="red-txt">password must be at least 6 digit</span>
                                  </label>
                                  {{Form::password('password',[
                                    'class'=>"form-control",
                                    'required'=>"required",
                                    'id'=>"register-password",
                                    'placeholder'=>"Enter Password",
                                    'minlength'=>6  
                                  ])}}
                              </div>
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="form-row">
                              <div class="col-md-12">
                                  <label for="register-password-confirmation">PASSWORD CONFIRM
                                      <span class="red-txt">*</span>
                                  </label>
                                  {{Form::password('password_confirmation',[
                                    'class'=>"form-control",
                                    'required'=>"required",
                                    'id'=>"register-password-confirmation",
                                    'data-parsley-equalto'=>"#register-password",
                                    'data-parsley-equalto-message'=>'This value should be the same as Password value.',
                                    'placeholder'=>"Enter Confirm Password" ,
                                    'minlength'=>6
                                  ])}}
                                  @if ($errors->has('password_confirmation'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('password_confirmation') }}</strong>
                                      </span>
                                  @endif  
                              </div>
                          </div>
                      </div>
                      <div class="col-12 col-md-12 text-center">
                          <div class="form-check"> 
                            <input class="form-check-input" type="checkbox" value="" id="agreement" required="required">
                            <label class="form-check-label" for="agreement">
                              Term & Condition
                            </label>
                            <a href="#" data-toggle="modal" data-target="#agree_modal">Readme</a>
                          </div>
                      </div>

                  </div>
              </div>
          </div>
          <div class="col-12 col-md-12 mt-4 text-center">
              <input type="submit" id="" value="REGISTER" class="btn btn-dark" style="width: 80%;background-color: #001f2c;">
          </div>  
      </form>
      <div class="text-center"> 
          <a class="d-block small mt-3" href="{{URL::to('/member/login')}}">Login Page</a>
          <br>
      </div>

  </div>
</div>


<!-- Modal Agree -->
<div class="modal fade" id="agree_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Term & Condition</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          Terms & Conditions
I confirm that I have read and understand the Terms and Conditions included in the FIC's Terms of Token Sale and that I expressly accept all terms, conditions, obligations, affirmations, representations and warranties described in these Terms and agree to be bound by them. At the time of FIC Token Initial Coin Offering (ICO), the Securities and Exchange Commission (SEC) in Thailand is currently in the process of drafting and implementing regulations regarding initial coin offerings. When the regulation has been enforced, the compliance of such regulations will be considered to be maintained. The blockchain and AI (Artificial Intelligence) technology, including the issue of tokens and its usage, is a brand-new concept in jurisdictions. Current regulations may not cover every aspects of this emerging technologies. This may result in change of the FIC's strategy and  such may conflict with the FIC Application product setup, including but not limited to the loss of FIC Tokens. I am not eligible and I am not to purchase any tokens if I am a citizen or resident (tax or otherwise) of the People’s Republic of China (”PRC”), or the United States of America. Furthermore because of current regulatory
uncertainty and before additional information is publicly released on the matter, green card holders of the United States or citizens or residents (tax or otherwise) of the UnitedStates of America, or China, or other U.S., Person or PRC Person,
are not eligible to register as Continuous Contributors. That limits the ability of U.S., PRC persons to utilize tokens and participate in the future development of the Platform as well as in the distribution of proceeds.

I confirm that the information given in this form is true, complete and accurate.

To participate in Pre-ICO and ICO, you have to create a user account on our website first, then go through KYC (Know Your Customer) and you must have your Ethereum Wallet address in order to obtain the tokens.  
This verification must be completed and can be done any time until the end of the ICO periods. IF NOT WE WILL NOT TRANSFER THE TOKENS FROM THE SYSTEM TO YOUR ETHEREUM WALLET. The confirmation will not affect any purchase of tokens. Please mention this information to understand about our transaction regulations. 
Thank you. 
Flora FIC Team
      </div>
      <div class="modal-footer">
        <div class="col-12 col-md-12 text-center">
            <div class="form-check"> 
              <!-- <input class="form-check-input" type="checkbox" value="" id="agreement" required="required">
              <label class="form-check-label" for="agreement">
                Agreement
              </label> -->
              
            </div>
        </div>

        <div class="colo-md-12">
           <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('style')
<style>
.parsley-errors-list li.parsley-required {
    padding: 10px;
    color: #f00;
}
</style>
@endsection
@section('script')
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="{{asset('js/animate/particles.min.js')}}"></script>
<script src="{{asset('js/animate/particles_spider.js')}}"></script>
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" /> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> -->
{!! HTML::script('js/plugins/Parsley.js/dist/parsley.js'); !!}
<script>
    $(document).ready(function() {

      $('.register_call').click(function(event) {
        $('#login_box').hide('slow/400/fast', function() {
          $('#register_box').show('slow/400/fast', function() {
            
          });
        });
      });

      $('.login_call').click(function(event) {
        $('#register_box').hide('slow/400/fast', function() {
          $('#login_box').show('slow/400/fast', function() {
            
          });
        });
      });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#regis_submit').attr({
            disabled: 'disabled'
        });

        $('input[name=email]').keyup(function(event) {
            var email = $('input[name=email]').val();
            //var user = $('input[name=user]').val();
            
            //$('#chk_email').html(email);
            //$('#chk_user').html(user);

            var type = "email";

            $.post('/api/checkUserEmail', {type:type, email: email}, function(data) {

                if (data.stt !== 'success') {
                    $('#chk_email').html('<span class="red-txt">Email in used</span>');
                    $('#regis_submit').attr({disabled:'disabled'});
                    $('input[name=email_hidden]').attr({value:'1'});
                }else if(data.stt === 'success'){
                    $('#chk_email').html('<span class="green-txt">Email available</span>');
                    $('#regis_submit').removeAttr("disabled");
                    $('input[name=email_hidden]').attr({value:'0'});
                }

            },"json");

            /*var email_hidden = $('input[name=email_hidden').val();

            console.log(email_hidden);

            if (email_hidden == 1) {
                $('#regis_submit').attr({disabled:'disabled'});
            }

            if (email_hidden == 0) {
                $('#regis_submit').removeAttr("disabled");
            }*/


        });


    });
    
    $('#register-form').parsley();
</script>

@endsection


