<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
  <a class="navbar-brand" href="#"><img src="{{asset('images/fic-currency.png')}}" width="25px" style="margin-left: 5px"></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item {!! active_class_path(['cmscontrol'],'active') !!}" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{URL::to('cmscontrol')}}">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">FIC Order</span>
          </a>
        </li>
        <li class="nav-item {!! active_class_path(['cmscontrol/kyc'],'active') !!}" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{URL::to('cmscontrol/kyc')}}">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">KYC List</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>

      <ul class="navbar-nav ml-auto">
 
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="">{{Auth::user()->username}}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>