<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav" style="background-color: #0099cc;">
  <a class="navbar-brand" href="index.php"><img src="{{asset('images/fic-currency-1.png')}}" width="25px" style=""></a>
    <span style="color: #fff; font-weight: bold;">Your balance: 
    @if(Auth::user()->fic > 0)
        {{number_format(Auth::user()->fic,8,'.',',')}}
    @else
    0.00000000
    @endif
     FIC</span>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion" style="margin-top: -5px;">
        <!-- <li class="nav-item {!! active_class_path(['profile'],'active') !!}" data-toggle="tooltip" data-placement="right" title="logo"> -->
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="logo">
          <a class="navbar-brand d-none d-md-block d-lg-block text-center mt-2 mb-5" href="index.php"><img src="{{asset('images/fic-currency.png')}}" width="40px"></a>
        </li>
        <li class="nav-item {!! active_class_path(['/*'],'active') !!}" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{URL::to('/')}}">
            <i class="fa fa-fw fa-dashboard text-white"></i>
            <span class="nav-link-text text-white">Buy FIC Token</span>
          </a>
        </li>
        <li class="nav-item {!! active_class_path(['profile'],'active') !!}" data-toggle="tooltip" data-placement="right" title="Profile">
          <a class="nav-link" href="{{URL::to('/profile')}}">
            <i class="fa fa-university text-white" aria-hidden="true"></i>
            <span class="nav-link-text text-white">Profile</span>
          </a>
        </li>
        <li class="nav-item {!! active_class_path(['your-referrals'],'active') !!}" data-toggle="tooltip" data-placement="right" title="Charts">
          <a class="nav-link" href="{{URL::to('/your-referrals')}}">
            <i class="fa fa-users text-white" aria-hidden="true"></i>
            <span class="nav-link-text text-white">Your referrals</span>
          </a>
        </li>

        <li class="nav-item {!! active_class_path(['my-wallet'],'active') !!}" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="{{URL::to('/my-wallet')}}">
            <i class="fa fa-fw fa-table text-white"></i>
            <span class="nav-link-text text-white">Transaction</span>
          </a>
        </li>

        <li class="nav-item {!! active_class_path(['setting'],'active') !!}" data-toggle="tooltip" data-placement="right" title="Setting">
          <a class="nav-link" href="{{URL::to('/setting')}}">
            <i class="fa fa-fw fa-link text-white"></i>
            <span class="nav-link-text text-white">Setting</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-bars"></i>
          </a>
        </li>
      </ul>

      <ul class="navbar-nav ml-auto">
 
        <li class="nav-item">
          <a class="right_nav_a nav-link" data-toggle="modal" data-target="">{{Auth::user()->username}}</a>
        </li>
        <li class="nav-item">
          <a class="right_nav_a nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>


  <style type="text/css">
    .right_nav_a{
      color: #ffffff !important;
      font-weight: bold !important;
    }
    .right_nav_a:hover{
      color: #1561d4 !important;
    }
  </style>



