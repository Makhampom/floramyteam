<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
  <a class="navbar-brand" href="index.php"><img src="{{asset('images/fic-currency.png')}}" width="25px" style="margin-left: 5px"></a>
    <span style="color: #fff; font-weight: bold;">Your balance: 
    @if(Auth::user()->fic > 0)
        {{number_format(Auth::user()->fic,8,'.',',')}}
    @else
    0.00000000
    @endif
     FIC</span>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item {!! active_class_path(['/*'],'active') !!}" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{URL::to('/')}}">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Buy FIC Token</span>
          </a>
        </li>
        <li class="nav-item {!! active_class_path(['profile'],'active') !!}" data-toggle="tooltip" data-placement="right" title="Profile">
          <a class="nav-link" href="{{URL::to('/profile')}}">
            <i class="fa fa-university" aria-hidden="true"></i>
            <span class="nav-link-text">Profile</span>
          </a>
        </li>
        <li class="nav-item {!! active_class_path(['your-referrals'],'active') !!}" data-toggle="tooltip" data-placement="right" title="Charts">
          <a class="nav-link" href="{{URL::to('/your-referrals')}}">
            <i class="fa fa-users" aria-hidden="true"></i>
            <span class="nav-link-text">Your referrals</span>
          </a>
        </li>

        <li class="nav-item {!! active_class_path(['my-wallet'],'active') !!}" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="{{URL::to('/my-wallet')}}">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">My wallet</span>
          </a>
        </li>

        {{--<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Setting">
          <a class="nav-link" href="{{URL::to('/setting')}}">
            <i class="fa fa-fw fa-link"></i>
            <span class="nav-link-text">Setting</span>
          </a>--}}
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>

      <ul class="navbar-nav ml-auto">
 
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="">{{Auth::user()->username}}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>