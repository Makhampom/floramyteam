<style type="text/css">
@media (max-width: 767.98px) { 
  .show-mail {
      position: fixed; 
      z-index: 2000;
      left: 20px;
      bottom: -1em;
      width: 17em;
  }
    .call-mail {
      position: fixed;
      z-index: 2000;
      font-size: 0px;
      left: 30%;
      bottom: 5%;
  }
}
@media (min-width: 768px) { 
  .show-mail {
      position: fixed; 
      z-index: 2000; 
      right: 20px; 
      bottom: 2em;
      width: 20em;
  }
  .call-mail {
      position: fixed;
      z-index: 2000;
      font-size: 0px;
      right: 20px;
      bottom: 5%;
  }
}
</style>

<div class="col-12">
  <div class="row">
      <!-- <div class="col-md-9"></div> -->
      <div class="show-mail">
        <div id="show_mail_contact" class="card mx-auto" style="display: none;">
          <div class="card-header">
            <div class="row">
              <div class="col-9 col-md-9"><strong>Florafic Team</strong></div>
              <div class="mail_hide col-1 col-md-1 text-secondary"><i class="fa fa-window-minimize" aria-hidden="true"></i></div>
              <div class="mail_hide col-1 col-md-1 text-secondary"><i class="fa fa-times" aria-hidden="true"></i></div>
            </div>
          </div>
          <div class="card-body">
            <form>
              <div class="form-group">
                <label for="name">Name <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="" required="required" style="border-radius: 30px;">
              </div>
              <div class="form-group mt-3">
                <label for="email">Email <span class="text-danger">*</span></label>
                <input type="email" class="form-control" name="" required="required" style="border-radius: 30px;">
              </div>
              <div class="form-group">
                <label for="content">Content <span class="text-danger">*</span></label>
                <div>
                  <textarea name="" class="row3 form-control" required="required" style="border-radius: 15px;resize: none;"></textarea>
                </div>
              </div>
              <div class="text-center">
                <input type="submit" class="btn btn-danger btn-lg" value="Submit" style="border-radius: 30px;width: 60%;">
              </div>
            </form>
          </div>
        </div>

        <div id="call_mail_contact" class="text-center call-mail">
          <a href="#" class="btn btn-light" style="border-radius: 30px;">
           <i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email Us  
          </a>
        </div>
      </div>
    </div>
</div>




