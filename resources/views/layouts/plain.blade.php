<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>@yield('title')</title>
  <!-- Bootstrap core CSS-->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{asset('css/font-lato.css')}}">
  <!-- Custom fonts for this template-->
  <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">

  @yield('style')
</head>

<body class="font_latoregular" style="background-color: #324c66;">
  <div class="container">
      <div class="col-md-12" style="position: fixed; left: 0px; top: 0px; margin-left: 0px;">
        <div id="particles-js" style="padding-left: -15px; margin-left: -15px;"></div>
      </div>
    <div class="row"> 
      <div class="col-12 col-md-12 text-center d-sm-block d-md-none d-lg-none">
        <img src="{{asset('images/logo-horizontal.png')}}" class="" style="width: 100%;">
      </div>
      <div class="col-12 col-md-12 text-center d-none d-md-block d-lg-block">
        <img src="{{asset('images/logo-horizontal.png')}}" class="" style="width: 28%;">
      </div>
    </div>
    
    @yield('content')
    
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <!-- Core plugin JavaScript-->
  <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
  <script src="{{asset('js/mail_contact.js')}}"></script>

  @yield('script')
</body>

</html>