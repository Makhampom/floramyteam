<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>@yield('title')</title>
  <link rel="icon" type="image/png" href="{{asset('images/favicon.png')}}"/>
  <!-- Bootstrap core CSS-->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">
  <link href="{{asset('css/index-custom.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <script src="{{asset('js/mail_contact.js')}}"></script>

  @yield('style')
  <style type="text/css">

    body{
      font-family: 'Lato', sans-serif;
    }
    
    .top-em1{
      margin-top: 1em;
    }
    .top-em1-5 {
      margin-top: 1.5em;
    }

    .div-white{
      background-color: #fff; 
      margin-left: 1.5em; 
      margin-top: 2em; 
      border-radius: 5px;
    }
    .select-buy-currency img{
      width: 60px;
      margin-top: 1em;
    }

    a{
      color: #000;
    }

    a:hover{
      text-decoration: none;
    }

    .text-16{
      font-size: 14px;
      color: #000;
    }

    .txt-red{
      columns: #f00;
    }

    .arrow-convert img{
      width: auto;
    }

    .left-em-3{
      margin-left: 2em;
    }
    .mg-left4 {
      margin-left: 4em;
    }
    .currency-convert{
      width: 120px; 
      height: 176px;
      border-radius: 5px;
      margin-right: 5px;
    }
    textarea {
      resize: none !important;
    }
    .qr-img {
      width: 40px;
    }
    /* small down */
    @media (max-width: 767px) {
      .heigh2 {
        height: 55em;
      }
      .select-buy-currency{
      width: 140px; 
      height: 160px;
      background-color: #f1f5f8; 
      border-radius: 5px;
      }
      .text-lc {
        text-align: center!important;
      }
      .mg-left4 {
        margin-left: 2.5em;
      }
      .ml5 {
        margin-left: 2em;
      }
      .ex-img {
        width: 70px !important;
      }

    }
    /* medium up */
    @media (min-width: 768px) {
      .heigh1 {
        height: 30em;
      }
      .select-buy-currency{
      width: 172px; 
      height: 160px;
      background-color: #f1f5f8; 
      border-radius: 5px;
      }
      .mgl {
        margin-left: -3em;
      }
      .mg-top {
        margin-top: 3em;
      }
      .text-lc {
        text-align: left!important;
      }
      .ml5 {
        margin-left: 3em;
      }
      .box-buy {
        width: 260px !important; 
      }
    }
    /* 1200 up */
    @media (min-width: 1200px) {
      .mgl {
        margin-left: 3em;
      }
      .mg-top {
        margin-top: 2em;
      }
    }
    
    .img_small{
      width: 40%;
    }

    .img_large{
      width: 85%;
    }

  </style>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" style="color: #676565">
  <!-- Navigation-->
  @if(Auth::guard('web')->check())
    @include('partials._menu')
  @else
    @include('partials._admin_menu')
  @endif
  <div class="content-wrapper" style="background-color: #EFF5FB;">
    <div class="container-fluid">
      @yield('content')
      @extends('layouts.mail_contact')
      <!-- Example DataTables Card-->
      <div class="card mb-3" style="display: none;">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Table Example</div>
        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © florafic.io 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <!-- <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a> -->
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="{{URL::to('/member/logout')}}">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <!-- Page level plugin JavaScript-->
    <script src="{{asset('vendor/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{asset('js/sb-admin.min.js')}}"></script>
    <!-- Custom scripts for this page-->
    <script src="{{asset('js/sb-admin-datatables.min.js')}}"></script>
    <script src="{{asset('js/mail_contact.js')}}"></script>
    
    @yield('script')

  </div>
</body>

</html>
