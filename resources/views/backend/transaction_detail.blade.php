@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-6" style="margin-left: 1em; margin-top: 1em; color: #000; font-weight: bold; font-size: 1.5em; padding-left: 5px;">Calculate your investment amount</div>
</div>  
<form name="approveOrderFIC" id="approveOrderFIC" action="{{url('/cmscontrol/accept/order/'.$transaction->id)}}" method="POST">
    <div class="row">
    <div class="col-12 col-md-11 div-white mb-5">
        <!-- Example Bar Chart Card-->
        <div class="col-12 col-md-12" style="margin-top: 1em; margin-bottom: 1em; color: #000;">
        Transaction Order Buy FIC coins with {{$transaction->wallet->code}}
        </div>
        <div class="row text-center left-em-3">
          <div class="col-md-1"></div>
          <div class="col-md-4 select-buy-currency">
              <img src="{{asset('images/'.$transaction->wallet->logo)}}" style="margin-top: 1.5em;">
              <!-- <img src="img/buy-with-eos.png" style="margin-top: 1.5em;"> -->
              <div class="top-em1">{{$transaction->wallet->code}}</div>
              <!-- <div class="top-em1">EOS</div> -->
          </div>
          <div class="col-md-2 arrow-convert align-center d-xl-block d-none d-md-block">
              <img src="{{asset('images/arrow-convert.png')}}" class="mt-4">
              <!-- <img src="img/arrow-convert.png" class="mt-4"> -->
          </div>
          <!-- show extra small -->
          <div class="d-xl-none d-sm-none d-md-none col-12 mg-left4">
              <div class="arrow-convert">
                <img src="{{asset('images/arrow-convert-down.png')}}">
                <!-- <img src="img/arrow-convert-down.png"> -->
              </div>
          </div>
          <div class="col-md-4 select-buy-currency">
              <img src="{{asset('images/fic-currency.png')}}" class="mt-4" style="width: 140px;height: 140px;">
              <!-- <img src="img/fic-currency.png" style="width: 140px;height: 140px;" class="mt-4"> -->
              <div class="top-em1">FIC</div>
          </div>
          <div class="col-md-1"></div>
        </div>


    <!-- ******************** -->
    <div class="row left-em-3 mt-4">
        <div class="col-md-1"></div>
        <div class="col-12 col-md-4" style="padding: 0px;">
            <h6 class="ex-left">Buyer send:</h6>
                <div class="col-md-12">
                  <div class="input-group">
                      <div id="sender_box" class="input-group-prepend">
                        <input type="radio" name="send_type" value="1" checked="checked">&nbsp;
                        {{$transaction->send_value}}
                        <!-- 10.00000000 --> &nbsp;&nbsp;
                        <span class="" style="padding-left: 1px; padding-right: 1px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;">{{$transaction->wallet->code}} <!-- <stong>EOS</stong> --></span>
                      </div>
                  </div>
                </div>
            <h6 class="ex-left mgtop">Actual recieve:</h6> 
              <div class="col-md-12">
                <div class="input-group"> 
                    <div id="actually_box" class="input-group-prepend">
                      <input type="radio" name="send_type" value="2" class="mt-2">&nbsp;
                      <input type="number" name="actually_coin" class="form-control" id="actually_coin" data-per-fic="{{$transaction->bonus_rate}}" value="0.00000000">
                      <span class="input-group-text" style="padding-left: 1px; padding-right: 1px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"> {{$transaction->wallet->code}} </span>
                    </div>
                </div>
              </div>
            <h6 class="ex-left mt-2">bonuses:</h6>
            <div class="input-group ex-left text-danger">
                {{$transaction->bonus_rate}} <!-- 15 -->
            </div>
        </div>
        <div class="col-md-2 arrow-convert align-center"></div>
        <div class="col-12 col-md-4" style="padding: 0px;">
            <h6 class="ex-left">Buyer receive:</h6>
            <div class="col-md-12">
              <div class="input-group">
                <div class="input-group-prepend">
                  <input type="number" name="fic_receive" class="form-control" id="fic_receive" value="{{$transaction->fic_receive}}">
                  <span class="input-group-text" style="padding-left: 1px; padding-right: 1px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;">FIC</span>
                </div>
              </div>
            </div>
            <h6 class="ex-left mt-2">Including bonuses:</h6>
            <div class="col-md-12">
              <div class="input-group">
                <div class="input-group-prepend">
                  <input type="number" name="all_fic_receive" class="form-control" id="all_fic_receive" value="{{$transaction->all_fic_receive}}">
                  <div class="input-group-text" style="padding-left: 1px; padding-right: 1px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;">FIC</div>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 1em;"></div>
    <div class="col-2 col-md-2"></div>
    <div class="col-10 col-md-10">
        <h4>Transaction Imformation</h4>
        <table>
            <tr>
                <td>Buyer Name</td>
                <td>{{$transaction->user->username}}/td>
            </tr>
            <tr>
                <td>Buyer Email</td>
                <td>{{$transaction->user->email}}</td>
            </tr>
            <tr>
                <td>Order Date</td>
                <td>{{$transaction->created_at->format('d-m-Y H:i:s')}}</td>
            </tr>
            <tr>
                <td>Transaction ID</td>
                <td>{{$transaction->transfer_code}}</td>
            </tr>
            <tr>
                <td>To Wallet</td>
                <td>{{$transaction->to_wallet}}</td>
            </tr>
            <tr>
                <td>Buyer ETH Wallet</td>
                <td>{{$transaction->user->eth_wallet}}</td>
            </tr>
        </table>
    </div>
    <div class="row" style="margin-top: 2em;margin-bottom: 1em;">
        <div class="col-md-12 text-center">
            <br>
            <input type="hidden" name="id" value="{{$transaction->id}}">
            <button type="button" id="submit_order" class="btn btn-warning btn-lg" style="color: #fff;">
            <i class="fa fa-shopping-cart" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;Approve Order
            </button>
            <button type="button" id="back" class="btn btn-danger btn-lg" style="color: #fff;" onclick="window.history.go(-1); return false;">
            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;Back
            </button>
        </div>
        <div class="col-md-5"></div>
    </div>
</div>
{{ csrf_field() }}
</form>
@endsection
@section('script')
<script src="{{asset('js/jquery.plugin.js')}}"></script>
<script src="{{asset('js/jquery.maxlength.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#submit_order').click(function(){
            //alert($('input[name="send_type"]:checked').val());
            //alert($('input[name="actually_coin"]').val());
            if($('input[name="send_type"]:checked').val() == 2 && $('input[name="actually_coin"]').val() == false){
                alert('Error');
            }else{
                $('#approveOrderFIC').submit();
            }
        });
        $('input[name="send_type"]').change(function(){
            if($(this).val() == 2){
                $('#actually_box').removeAttr('disabled');
                $('#sender_box').attr('disabled',true);
            }else{
                $('#sender_box').removeAttr('disabled');
                $('#actually_box').attr('disabled',true);
            }
        });
        $('#actually_coin').change(function(){
            //alert($(this).val());
        })
    });
</script>
@endsection