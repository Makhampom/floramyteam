@extends('layouts.default')
@section('content')
@php($users = App\Models\User::whereNotNull('kyc_file')->orderBy('kyc_status')->orderBy('created_at','desc')->with('country')->get())
<div class="row">
    <div class="col-md-6 col-sm-12 col-xs-12" style="margin-left: 1em; margin-top: 1em; color: #000; font-weight: bold; font-size: 1.5em; padding-left: 5px;">Flora FIC Admin Control Panel
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 2em; background-color: white;">
        <div class="col-md-12" style="margin-top: 1em; margin-bottom: 1em; color: #000; font-size: 1em;">
            <div class="row">
                <div class="col-md-12 mt-3 mb-3 pt-3 pb-3" style="font-size: 1.2em; background-color: #0099cc; color: #ffffff;"> <strong>KNOW YOUR CUSTOMER</strong> </div>    
                <div class="col-md-12"><hr></div>
            </div>
        </div>
        @if($users->isNotEmpty())
        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 2em; background-color: white;">
            <div class="col-md-12 mt-3" style="margin-top: 1em; margin-bottom: 1em; color: #000; font-size: 12px; padding: 0px;">
                <table id="example" class="col-md-12 table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Name</th>
                            <th scope="col">Surname</th>
                            <th scope="col">Country</th>
                            <td scope="col">email</td>
                            <th scope="col">kyc</th>
                            <th scope="col">Status</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $key => $value)
                        <tr>
                            <td>{{$key+=1}}</td>
                            <td>{{$value->name}}</td>
                            <td>{{$value->surname}}</td>
                            <td>{{($value->country)?$value->country->name:'-'}}</td>
                            <td>{{$value->email}}</td>
                            <td align="center">
                                @if(File::extension(asset('images/kyc/'.$value->kyc_file)) == 'pdf')
                                    <a href="{{asset('images/kyc/'.$value->kyc_file)}}" target="_blank">
                                        <img src="{{asset('images/pdf_icon.svg')}}" width="50px">
                                        <p>View PDF</p>
                                    </a>
                                @else
                                    <img src="{{asset('images/kyc/'.$value->kyc_file)}}" width="100px">
                                @endif
                            </td>
                            <td>
                                @if($value->kyc_status != 0)
                                {!!$value->getKycStatusDisplay(1)!!}
                                @else
                                <a href="{{url('/cmscontrol/kyc/approve/'.$value->code)}}">
                                    <button class="btn btn-sm btn-success">Approve</button>
                                </a>
                                <a href="{{url('/cmscontrol/kyc/reject/'.$value->code)}}">
                                    <button class="btn btn-sm btn-danger"><i class="fa fa-times-circle"></i></button>
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th scope="col">No.</th>
                            <th scoonpe="col">Name</th>
                            <th scope="col">Surname</th>
                            <th scope="col">Country</th>
                            <td scope="col">email</td>
                            <th scope="col">kyc</th>
                            <th scope="col">Status</th>                            
                        </tr>
                    </tfoot>
                    
                </table>
            </div>
        </div>
        @endif
    </div>
    <hr/>
        
</div>

@endsection
@section('style')
<link href="{{asset('css/index-custom.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection