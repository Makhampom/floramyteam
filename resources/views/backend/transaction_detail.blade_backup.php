@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-6" style="margin-left: 1em; margin-top: 1em; color: #000; font-weight: bold; font-size: 1.5em; padding-left: 5px;">Calculate your investment amount</div>
</div>  
<form name="approveOrderFIC" id="approveOrderFIC" action="{{url('/cmscontrol/accept/order/'.$transaction->id)}}" method="POST">
<div class="row">
    <div class="col-md-11 div-white">
        <!-- Example Bar Chart Card-->
        <div class="col-md-12" style="margin-top: 1em; margin-bottom: 1em; color: #000;">
        Transaction Order Buy FIC coins with {{$transaction->wallet->code}}
        </div>
        <div class="row text-center left-em-3">
        <div class="col-md-4 select-buy-currency">
            <img src="{{asset('images/'.$transaction->wallet->logo)}}" style="margin-top: 1.5em;">
            <div class="top-em1">{{$transaction->wallet->code}}</div>
        </div>
        <div class="col-md-2 arrow-convert align-center">
            <img src="{{asset('images/arrow-convert.png')}}">
        </div>
        <div class="col-md-4 select-buy-currency">
            <img src="{{asset('images/fic-currency.png')}}">
            <div class="top-em1">FIC</div>
        </div>
    </div>
    <!-- ******************** -->
    <div class="row left-em-3">
        <div class="col-md-4" style="padding: 0px;">
            <h6>Buyer send:</h6>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div id="sender_box">
                        <input type="radio" name="send_type" value="1" checked="checked">
                        {{$transaction->send_value}}
                        <div class="input-group-text" style="padding-left: 1px; padding-right: 1px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;">{{$transaction->wallet->code}}</div>
                    </div>
                    <div id="actually_box">
                        <input type="radio" name="send_type" value="2">
                        <input type="number" name="actually_coin" class="form-control" id="actually_coin" data-per-fic="{{$transaction->bonus_rate}}" value="0.00000000" style="font-size: 13px;">
                        <div class="input-group-prepend">
                            <div class="input-group-text" style="padding-left: 1px; padding-right: 1px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;">{{$transaction->wallet->code}}</div>
                        </div>
                    </div>
                </div>
            </div>
            <h6>bonuses:</h6>
            <div class="input-group">
                {{$transaction->bonus_rate}}%
            </div>
        </div>
        <div class="col-md-2 arrow-convert align-center"> </div>
        <div class="col-md-4" style="padding: 0px;">
            <h6>Buyer receive:</h6>
            <div class="input-group">
                {{$transaction->fic_receive}}
                <div class="input-group-prepend">
                    <div class="input-group-text" style="padding-left: 1px; padding-right: 1px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;">FIC</div>
                </div>
            </div>
            <h6>Including bonuses:</h6>
            <div class="input-group">
                {{$transaction->all_fic_receive}}
                <div class="input-group-prepend">
                    <div class="input-group-text" style="padding-left: 1px; padding-right: 1px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;">FIC</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 1em;"></div>
    <div>
        <h4>Transaction Imformation</h4>
        <table>
            <tr>
                <td>Buyer Name</td>
                <td>{{$transaction->user->username}}/td>
            </tr>
            <tr>
                <td>Buyer Email</td>
                <td>{{$transaction->user->email}}</td>
            </tr>
            <tr>
                <td>Order Date</td>
                <td>{{$transaction->created_at->format('d-m-Y H:i:s')}}</td>
            </tr>
            <tr>
                <td>Sender Wallet</td>
                <td>{{$transaction->from_wallet}}</td>
            </tr>
            <tr>
                <td>To Wallet</td>
                <td>{{$transaction->to_wallet}}</td>
            </tr>
            <tr>
                <td>Buyer ETH Wallet</td>
                <td>{{$transaction->user->eth_wallet}}</td>
            </tr>
        </table>
    </div>
    <div class="row" style="margin-top: 2em;margin-bottom: 1em;">
        <div class="col-md-12 text-center">
            <br>
            <input type="hidden" name="id" value="{{$transaction->id}}">
            <button type="button" id="submit_order" class="btn btn-warning btn-lg" style="color: #fff;">
            <i class="fa fa-shopping-cart" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;Approve Order
            </button>
            <button type="button" id="back" class="btn btn-danger btn-lg" style="color: #fff;" onclick="window.history.go(-1); return false;">
            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;Back
            </button>
        </div>
        <div class="col-md-5"></div>
    </div>
</div>
{{ csrf_field() }}
</form>
@endsection
@section('script')
<script src="{{asset('js/jquery.plugin.js')}}"></script>
<script src="{{asset('js/jquery.maxlength.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#submit_order').click(function(){
            //alert($('input[name="send_type"]:checked').val());
            //alert($('input[name="actually_coin"]').val());
            if($('input[name="send_type"]:checked').val() == 2 && $('input[name="actually_coin"]').val() == false){
                alert('Error');
            }else{
                $('#approveOrderFIC').submit();
            }
        });
        $('input[name="send_type"]').change(function(){
            if($(this).val() == 2){
                $('#actually_box').removeAttr('disabled');
                $('#sender_box').attr('disabled',true);
            }else{
                $('#sender_box').removeAttr('disabled');
                $('#actually_box').attr('disabled',true);
            }
        });
        $('#actually_coin').change(function(){
            //alert($(this).val());
        })
    });
</script>
@endsection