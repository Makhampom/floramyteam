@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-6 col-sm-12 col-xs-12" style="margin-left: 1em; margin-top: 1em; color: #000; font-weight: bold; font-size: 1.5em; padding-left: 5px;">Flora FIC Admin Control Panel</div>
    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 2em; background-color: white;">
        <div class="col-md-12" style="margin-top: 1em; margin-bottom: 1em; color: #000; font-size: 1em;">
            <div class="row">
                <div class="col-md-12 mt-3 mb-3 pt-3 pb-3" style="font-size: 1.2em; background-color: #0099cc; color: #ffffff;"> <strong>FIC Balance : </strong> {{number_format(App\Models\User::whereNotNull('fic')->sum('fic'),8,'.',',')}} FIC</div>    
                <div class="col-md-12"><hr></div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 2em; background-color: white;">
        @php($transaction = App\Models\Transaction::whereNotNull('transfer_code')->where('status','!=',99)->orderBy('created_at','desc')->get())
        @if($transaction->isNotEmpty())
        @php($i = 1)
            <div class="col-md-12 mt-3" style="margin-top: 1em; margin-bottom: 1em; color: #000; font-size: 12px; padding: 0px;">
                <table id="example" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Date</th>
                            <th scope="col">Type</th>
                            <th scope="col">Send</th>
                            <th scope="col">FIC Recive</th>
                            <th scope="col">FIC Bonus</th>
                            <th scope="col">KYC Status</th>
                            <th scope="col">Status</th>
                            <th colspan="2" scope="col" class="text-center">Opt</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Date</th>
                            <th scope="col">Type</th>
                            <th scope="col">Send</th>
                            <th scope="col">FIC Recive</th>
                            <th scope="col">FIC Bonus</th>
                            <th scope="col">KYC Status</th>
                            <th scope="col">Status</th>
                            <th colspan="2" scope="col" class="text-center">Opt</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach($transaction as $key => $data)
                            @include('backend.partials._transaction_list', ['data'=>$data,'order'=>$i])
                            @php($i++)
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
        </div>
        <hr/>
        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 2em; background-color: white;">
        Waiting Buyer comfirm the transaction id
        @php($waitingTransaction = App\Models\Transaction::whereNull('transfer_code')->where('status','!=',99)->orderBy('created_at','desc')->get())
        @if($waitingTransaction->isNotEmpty())
        @php($i = 1)
            <div class="col-md-12 mt-3" style="margin-top: 1em; margin-bottom: 1em; color: #000; font-size: 12px; padding: 0px;">
                <table id="example" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Date</th>
                            <th scope="col">Send To Address</th>
                            <th scope="col">Transaction ID</th>
                            <th scope="col">Type</th>
                            <th scope="col">Send</th>
                            <th scope="col">FIC Recive</th>
                            <th scope="col">FIC Bonus</th>
                            <th scope="col">KYC Status</th>
                            <th scope="col">Status</th>
                            <th colspan="2" scope="col" class="text-center">Opt</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Date</th>
                            <th scope="col">Send To Address</th>
                            <th scope="col">Transaction ID</th>
                            <th scope="col">Type</th>
                            <th scope="col">Send</th>
                            <th scope="col">FIC Recive</th>
                            <th scope="col">FIC Bonus</th>
                            <th scope="col">KYC Status</th>
                            <th scope="col">Status</th>
                            <th colspan="2" scope="col" class="text-center">Opt</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach($waitingTransaction as $key => $data)
                            @include('backend.partials._transaction_waiting_list', ['data'=>$data,'order'=>$i])
                            @php($i++)
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
        </div>                
    </div>        
</div>  


@endsection
@section('style')
<link href="{{asset('css/index-custom.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('script')
<script>
    $('a[data-toggle="tooltip"]').tooltip({
        animated: 'fade'
    });
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection