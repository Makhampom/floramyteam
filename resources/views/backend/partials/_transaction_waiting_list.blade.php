<tr>
    <th scope="row">{{$order}}</th>
    <td>{{$data->created_at->format('d-m-Y H:i:s')}}</td>
    <td>{{$data->to_wallet}}</td>
    <td>{{($data->transfer_code)?$data->transfer_code:'waiting'}}</td>
    <td>{{$data->wallet->code}}</td>
    <td>{{decimalMoneyFormat($data->actual_send)}} ({{decimalMoneyFormat($data->send_value)}})</td>
    <td>{{decimalMoneyFormat($data->actual_fic)}} ({{decimalMoneyFormat($data->fic_receive)}})</td>
    <td>{{decimalMoneyFormat($data->actual_bonus)}} ({{decimalMoneyFormat($data->fic_bonus)}})</td>
    <td>
        @if($data->user->kyc_file != '')
        <a data-toggle="tooltip" data-html="true" data-placement="left" title="<img src='{{asset('images/kyc/'.$data->user->kyc_file)}}' width='100px' />">
        @endif
        {!!$data->user->getKycStatusDisplay(1)!!}
        @if($data->user->kyc_file != '')
        </a>
        @endif
    </td>
    <td>{{$data->getTransactionStatusView()}}</td>
    <td>
        @if($data->getTransactionStatusView()=='Pending')
            <a href="{{route('admin.transaction.reject', [$data->id])}}">
                <button class="btn btn-sm btn-danger"><i style="font-size: 14px;" class="fa fa-times-circle"></i></button>
            </a>
        @endif
        
    </td>
</tr>