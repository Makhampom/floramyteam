<tr>
    <th scope="row">{{$order}}</th>
    <td>{{$data->created_at->format('d-m-Y H:i:s')}}</td>
    <td>{{$data->to_wallet}}</td>
    <td>{{$data->transfer_code}}</td>
    <td>{{$data->wallet->code}}</td>
    <td>{{decimalMoneyFormat($data->actual_send)}} ({{decimalMoneyFormat($data->send_value)}})</td>
    <td>{{decimalMoneyFormat($data->actual_fic)}} ({{decimalMoneyFormat($data->fic_receive)}})</td>
    <td>{{decimalMoneyFormat($data->actual_bonus)}} ({{decimalMoneyFormat($data->fic_bonus)}})</td>
    <td>
        @if($data->user->kyc_file != '' && File::extension(asset('images/kyc/'.$data->user->kyc_file)) != 'pdf')
        <a data-toggle="tooltip" data-html="true" data-placement="left" title="<img src='{{asset('images/kyc/'.$data->user->kyc_file)}}' width='100px' />">
        @endif
        @if($data->user->kyc_file == '')
        <p class="text-danger">No</p>
        @else
        {!!$data->user->getKycStatusDisplay(1)!!}
        @endif
        @if($data->user->kyc_file != '' && File::extension(asset('images/kyc/'.$data->user->kyc_file)) != 'pdf')
        </a>
        @endif
    </td>
    <td>{{$data->getTransactionStatusView()}}</td>
    <td>
        <a href="{{URL::to('cmscontrol/transactionData/'.$data->id)}}"><button class="btn btn-sm btn-warning">{{($data->status == 0)?'Edit':'View'}}</button></a>
    </td>
    <td>
        @if($data->getTransactionStatusView()=='Pending')
            <a href="{{route('admin.transaction.reject', [$data->id])}}">
                <button class="btn btn-sm btn-danger"><i style="font-size: 14px;" class="fa fa-times-circle"></i></button>
            </a>
        @endif
        
    </td>
</tr>