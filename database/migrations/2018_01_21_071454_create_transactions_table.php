<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('wallet_type')->unsigned();
            $table->foreign('wallet_type')->references('id')->on('exchanges');
            //$table->string('from_wallet')->nullable();
            $table->string('transfer_code')->nullable();
            $table->string('to_wallet');
            $table->decimal('send_value',13,8);
            $table->integer('fic_rate')->default(0);
            $table->integer('bonus_rate')->default(0);
            $table->decimal('fic_receive',20,8)->nullable();
            $table->decimal('fic_bonus',20,8)->nullable();
            $table->decimal('ttl_receive',20,8)->nullable();
            $table->decimal('actual_send',20,8)->nullable();
            $table->decimal('actual_fic',20,8)->nullable();
            $table->decimal('actual_bonus',20,8)->nullable();
            $table->decimal('actual_fic_recieve',20,8)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
