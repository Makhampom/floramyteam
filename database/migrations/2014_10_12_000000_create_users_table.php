<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('surname');
            $table->string('code')->nullable();
            $table->string('email')->unique();
            $table->string('kyc_file')->nullable();
            $table->string('kyc_status')->default(0);
            $table->string('password');
            $table->string('eth_wallet')->nullable();
            $table->string('ref_count')->nullable();
            $table->string('fic')->nullable();
            $table->string('verify_code')->nullable();
            $table->string('verify')->nullable();
            $table->tinyInteger('google2fa_status')->default(0);
            $table->string('google2fa_secret',255)->nullable();
            $table->string('address',255)->nullable();
            $table->string('city',255)->nullable();
            $table->integer('countries_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
