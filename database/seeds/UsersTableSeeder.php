<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => str_random(10),
            'surname' => str_random(10),
            'email' => 'admin@gmail.com',
            'password' => bcrypt('uuuuuu8989'),
            'verify_code' => str_random(10),
            'verify' => 1
        ]);

        $user = DB::table('users')
                ->latest()
                ->first();
        DB::table('users')->where('id',$user->id)->update(['code' => getToken('6',$user->id)]);        
    }
}
