<?php

use Illuminate\Database\Seeder;

class ExchangesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('exchanges')->insert([
            'name' => 'Bitcoin',
            'code' => 'BTC',
            'rate' => 166667,
            'logo' => 'buy-with-btc.png',
            'bonus' => 30,
            'wallet' => '1DRMjt9HLfu92KAZxXoaVNMwLr43tCSCAh',
            'wallet_qr' => 'btc-qr.png'

        ]);
        DB::table('exchanges')->insert([
            'name' => 'Ethereum',
            'code' => 'ETH',
            'logo' => 'buy-with-eth.png',
            'rate' => 16667,
            'bonus' => 30,
            'wallet' => '0xA086c3f0056e17604Df72635B7A682e6220945d5',
            'wallet_qr' => 'eth-omg-eos-qr.png'
        ]);
        DB::table('exchanges')->insert([
            'name' => 'Bitcoin Cash',
            'code' => 'BCH',
            'rate' => 1000,
            'logo' => 'buy-with-bch.png',
            'bonus' => 15,
            'wallet' => '1CLaaXdeXhEVLuWMV1vcmQVN5YqYDPPnXV',
            'wallet_qr' => 'bch-qr.png'
        ]);
        DB::table('exchanges')->insert([
            'name' => 'Dashcoin',
            'code' => 'Dash',
            'rate' => 10000,
            'logo' => 'buy-with-dash.png',
            'bonus' => 30,
            'wallet' => 'XwLYAG51uwXhHHMUpCDhkG64hVXcHUSwRu',
            'wallet_qr' => 'dash-qr.png'
        ]);
        DB::table('exchanges')->insert([
            'name' => 'EOScoin',
            'code' => 'EOS',
            'rate' => 167,
            'logo' => 'buy-with-eos.png',
            'bonus' => 30,
            'wallet' => '0xA086c3f0056e17604Df72635B7A682e6220945d5',
            'wallet_qr' => 'eth-omg-eos-qr.png'
        ]);
        DB::table('exchanges')->insert([
            'name' => 'Litecoin',
            'code' => 'LTC',
            'rate' => 2333,
            'logo' => 'buy-with-ltc.png',
            'bonus' => 30,
            'wallet' => 'LUxML49FPPYAkEH7vfSW6bwG8QtntDf3Pz',
            'wallet_qr' => 'ltc-qr.png'
        ]);
        DB::table('exchanges')->insert([
            'name' => 'Omese Go',
            'code' => 'OMG',
            'rate' => 250,
            'logo' => 'buy-with-omg.png',
            'bonus' => 30,
            'wallet' => '0xA086c3f0056e17604Df72635B7A682e6220945d5',
            'wallet_qr' => 'eth-omg-eos-qr.png'
        ]);
        DB::table('exchanges')->insert([
            'name' => 'Salt',
            'code' => 'SALT',
            'rate' => 83,
            'logo' => 'buy-with-salt.png',
            'bonus' => 30,
            'wallet' => '0xA086c3f0056e17604Df72635B7A682e6220945d5',
            'wallet_qr' => 'salt-qr.png'
        ]);

    }
}
