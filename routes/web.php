<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/testMail', 'frontend\PageController@testmail');
Route::get('/', 'frontend\PageController@index')->middleware('auth');
Route::get('/order/{type}', 'frontend\OrderController@index',function($type){})->middleware('auth');
Route::post('/order/confirmTransacrion', 'frontend\OrderController@confirm')->middleware('auth');
Route::post('/order/{type}', 'frontend\OrderController@store',function($type){})->middleware('auth');

Route::get('/profile', 'frontend\ProfileController@index')->middleware('auth');
Route::post('/profile/update', 'frontend\ProfileController@update')->middleware('auth');
Route::post('/profile/changePassword', 'frontend\ProfileController@changePassword')->middleware('auth');
Route::post('/profile/kycUpload', 'frontend\ProfileController@uploadKYC')->middleware('auth');

Route::get('/your-referrals', 'frontend\ReferralController@index')->middleware('auth');

Route::get('/my-wallet', 'frontend\WalletController@index')->middleware('auth');
Route::post('/setWallet', 'frontend\WalletController@setWallet')->middleware('auth');

Route::get('/setting', 'frontend\SettingController@index')->middleware('auth');
Route::get('/2fa/enable', 'frontend\Google2FAController@enableTwoFactor');
Route::get('/2fa/validate', 'frontend\Google2FAController@getValidateToken');

Route::get('/2fa/testValidate', 'frontend\Google2FAController@test2FAToken');
Route::post('/2fa/getTFAData', 'frontend\Google2FAController@getTFAToken');
Route::post('/2fa/validate', ['middleware' => 'throttle:5', 'uses' => 'frontend\Google2FAController@postValidateToken']);
Route::post('/2fa/disable', 'frontend\Google2FAController@disableTwoFactor');

Route::prefix('api')->group(function() {
    Route::post('/checkUserEmail', 'API\AjaxController@checkUserEmail');
});

Route::prefix('api')->group(function() {
    Route::post('/chkExistingEmail', 'API\AjaxController@chkExistingEmail');
});

Route::prefix('cmscontrol')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'backend\AdminController@index')->name('admin.dashboard')->middleware('auth:admin');
    Route::get('/transactionData/{id}', 'backend\AdminController@viewTransactionData',function($id){})->name('admin.transaction.data')->middleware('auth:admin');
    Route::post('/accept/order/{id}', 'backend\AdminController@acceptOrder')->middleware('auth:admin');
    Route::get('/order/reject/{id}', 'backend\AdminController@rejectOrder',function($id){})->name('admin.transaction.reject')->middleware('auth:admin');
    
    Route::get('/kyc','backend\AdminController@viewKyc')->middleware('auth:admin');
    Route::get('/kyc/approve/{id}','backend\AdminController@approveKyc',function($id){})->middleware('auth:admin');
    Route::get('/kyc/reject/{id}','backend\AdminController@rejectKyc',function($id){})->middleware('auth:admin');
});

Route::get('/member/login', 'Auth\UserLoginController@showLoginForm')->name('login');
Route::post('/member/login', 'Auth\UserLoginController@login')->name('member.login.submit');
Route::get('/member/register',['use'=>'member.register',function(){
    return view('auth.register');
}]);
Route::get('/verify/{email}/{code}', 'Auth\ActivateUserController@index',function($email,$code){});

Route::post('/member/register', 'Auth\RegisterController@register');
Route::get('/member/verify_regis', 'Auth\RegisterController@verify_regis');
Route::get('/member/logout', 'Auth\UserLoginController@logout');


Route::get('/password/reset/', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
